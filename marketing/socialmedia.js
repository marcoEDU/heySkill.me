// created by
// Marco Bartsch



const facebook = require("./facebook.js")
const twitter = require("./twitter.js")
const mailchimp = require("./mailchimp.js")
const blog = require("./blogPost.js")
const slack = require("../database/slack.js");
const Airtable = require('airtable');
const config = require("../database/config.json")
const GoogleSheets = require("./GoogleSheets.js")
const cron = require("node-cron");
const fs = require("fs");
var algoliasearch = require("algoliasearch");
var client = algoliasearch(config.Algolia_ApplicationID, config.Algolia_APIkey);
var path = require('path');

function wait(ms) {
    return new Promise(resolve => {
        setTimeout(() => resolve(), ms);
    });
}

var base = new Airtable({
    apiKey: config.airtable_apiKey
}).base(config.airtable_baseID);

async function getSeconds(hms) {
    var a = hms.split(':'); // split it at the colons

    // minutes are worth 60 seconds. Hours are worth 60 minutes.
    var seconds = (+a[0]) * 60 * 60 + (+a[1]) * 60 + (+a[2]);

    return seconds
}

// checkregular()
// async function checkregular() {

// check every hour if surprising event happened or if milestone reached
cron.schedule("5 * * * *", async function () {
    let previous = require(path.join(__dirname, "/previousKeyNumbers.json"))
    let milestones = require(path.join(__dirname, "/milestones.json"))

    // get current database size from algolia
    let contentDatabaseSize
    let techDatabaseSize

    await client.listIndexes(function (err, content) {
        if (err) throw err;
        for (index of content.items) {
            if (index.name == "SkillMe") {
                contentDatabaseSize = index.entries
            } else if (index.name == "SkillMeTopics") {
                techDatabaseSize = index.entries
            }
        }
    });

    // get number of blog posts from algolia
    let blogPostNumber

    index = client.initIndex("specialHints");
    await index.browse('', {
        'filters': 'messageCategory: Blog'
    }, function browseDone(err, content) {
        if (err) {
            console.error(err);
        }

        blogPostNumber = content.hits.length;
    });

    // get number of sources from algolia
    let sourcesNumber
    let sources

    index = client.initIndex("SkillMe");
    await index.search({
        query: '',
        facets: 'IFcontentSource1'
    }).then(res => {

        sources = Object.keys(res.facets.IFcontentSource1);
        sourcesNumber = Object.keys(res.facets.IFcontentSource1).length;
    });


    // get number of content types from algolia
    let contentTypesNumber
    let contentTypes

    index = client.initIndex("SkillMe");
    await index.search({
        query: '',
        facets: 'contentcategory'
    }).then(res => {

        contentTypes = Object.keys(res.facets.contentcategory);
        contentTypesNumber = Object.keys(res.facets.contentcategory).length;
    });


    // get current data from google sheets
    let currently = {
        "Facebook Likes": parseInt(await GoogleSheets.getCell("overview", "C26")),
        "Twitter Follower": parseInt(await GoogleSheets.getCell("overview", "D26")),
        "Mailchimp Subscriber": parseInt(await GoogleSheets.getCell("overview", "E26")),
        "30 day active users (max)": parseInt(await GoogleSheets.getCell("overview", "C47")),
        "Average session duration (max)": await GoogleSheets.getCell("overview", "D47"),
        "content database size": await contentDatabaseSize,
        "tech database size": await techDatabaseSize,
        "guide articles": await blogPostNumber,
        "sources": await sourcesNumber,
        "contentTypes": await contentTypesNumber
    }

    await wait(10000)

    for (field in currently) {
        // check if milestone reached
        if (milestones[field]) {
            // check if value is not number => then remove $ or convert to sec
            let currentlValue
            if (typeof currently[field] == "number") {
                previousValue = await previous[field]
                currentlValue = await currently[field]
            } else if (currently[field].match(":")) {
                previousValue = await getSeconds(previous[field])
                currentlValue = await getSeconds(currently[field])
            } else if (currently[field].match("$")) {
                previousValue = await parseFloat(previous[field].replace("$", ""))
                currentlValue = await parseFloat(currently[field].replace("$", ""))
            } else {
                previousValue = await parseInt(previous[field])
                currentlValue = await parseInt(currently[field])
            }

            for (milestone of milestones[field]) {
                // if milestone reached...
                if (milestone == currentlValue || milestone < currentlValue) {
                    console.log(`
*🎉 It's time to celebrate!*
*🎉 Milestone reached:*
>>> ✅ ${milestone} '${field}'
(exact: ${currently[field]})`)

                    slack.post(`
*🎉 It's time to celebrate!*
*🎉 Milestone reached:*
>>> ✅ ${milestone} '${field}'
(exact: ${currently[field]})`)

                    // remove reached milestone & save file
                    function remove(array, element) {
                        return array.filter(e => e !== element);
                    }

                    milestones[field] = remove(milestones[field], milestone)
                    await fs.writeFile(path.join(__dirname, `/milestones.json`), JSON.stringify(milestones, null, 2));

                    // git push updated file => commit & push
                    // https://www.npmjs.com/package/simple-git
                    await require('simple-git')()
                        .push('origin', 'master')
                        .add(path.join(__dirname, `/milestones.json`))
                        .commit(`Milestones updated`, path.join(__dirname, `/milestones.json`))
                        .push('origin', 'master')
                }
            }
        }

        // check if critical change happened
        if (previous[field]) {
            // check if value is not number => then remove $ or convert to sec
            let previousValue
            let currentlValue
            if (typeof previous[field] == "number") {
                previousValue = await previous[field]
                currentlValue = await currently[field]
            } else if (previous[field].match(":")) {
                previousValue = await getSeconds(previous[field])
                currentlValue = await getSeconds(currently[field])
            } else if (previous[field].match("$")) {
                previousValue = await parseFloat(previous[field].replace("$", ""))
                currentlValue = await parseFloat(currently[field].replace("$", ""))
            }

            // if yes, check if massive increase (5% per hour)
            if ((currentlValue / previousValue) > 1.05) {
                let increase = `${(((currentlValue / previousValue) - 1) * 100).toFixed(1)}%`
                slack.post(
                    `🚀 Wow, '${field}' is growing fast -
>>> *📈 +${increase} over the last hour*
*📈 from ${field=="Average session duration (max)"?await previous[field]:previousValue} => ${field=="Average session duration (max)"?await currently[field]:currentlValue}*`)
            }

            // else check if massive decrease (5% per hour)
            else if ((currentlValue / previousValue) < 0.95) {
                let decrease = `${(((currentlValue / previousValue) - 1) * 100).toFixed(1)}%`
                slack.post(
                    `⚠️ Oh... '${field}' is decreasing fast -
>>> *📉 ${decrease} over the last hour*
*📉 from ${field=="Average session duration (max)"?await previous[field]:previousValue} => ${field=="Average session duration (max)"?await currently[field]:currentlValue}*

        *Take action!* 

        Check if website is working fine:
        https://heyskill.me

        Check if userflows can explain decrease:
        https://app.fullstory.com/ui/D3SY7/segments/newThisWeek/people/0

        Consider creating more social media posts:
        https://airtable.com/tbldu2ZwCffMR0NBj/viwyfkLTAtavZTLDN

        *Learn more:*

        Googlesheets://${config.GoogleSheetsKeyNumberURL}

        ${config.GoogleSheetsKeyNumberURL}`)
            }
            // else save current field value and check again next hour for change
            previous[field] = currently[field]
            await fs.writeFile(path.join(__dirname, `/previousKeyNumbers.json`), JSON.stringify(previous, null, 2));
        } else {
            // else save current field value and check again next hour for change
            previous[field] = currently[field]
            await fs.writeFile(path.join(__dirname, `/previousKeyNumbers.json`), JSON.stringify(previous, null, 2));
        }
    }
    // }
})

// send once a day notification that script is still running
cron.schedule("0 5 * * *", async function () {
    console.log("✅ All fine, Social Media script is running.")
    slack.post("✅ All fine, Social Media script is running.")
})

// send Slack report once a day
cron.schedule("0 6 * * *", async function () {

    // dailyUpdate()
    // async function dailyUpdate() {
    try {
        // get latest data from google sheets
        let today = {
            "Facebook Likes": parseInt(await GoogleSheets.getCell("overview", "C26")),
            "Twitter Follower": parseInt(await GoogleSheets.getCell("overview", "D26")),
            "Mailchimp Subscriber": parseInt(await GoogleSheets.getCell("overview", "E26")),
            "30 day active users (max)": parseInt(await GoogleSheets.getCell("overview", "C47")),
            "Average session duration (max)": await GoogleSheets.getCell("overview", "D47"),
        }

        let yesterday = {
            "30 day active users (max)": parseInt(await GoogleSheets.getCell("overview", "C57")),
            "Average session duration (max)": await GoogleSheets.getCell("overview", "D57"),
        }

        let goal = {
            "up to": await GoogleSheets.getCell("overview", "B25"),
            "Facebook Likes": parseInt(await GoogleSheets.getCell("overview", "C25")),
            "Twitter Follower": parseInt(await GoogleSheets.getCell("overview", "D25")),
            "Mailchimp Subscriber": parseInt(await GoogleSheets.getCell("overview", "E25")),
            "30 day active users (max)": parseInt(await GoogleSheets.getCell("overview", "C46")),
            "Average session duration (max)": await GoogleSheets.getCell("overview", "D46"),
        }

        let today30dUser = await today["30 day active users (max)"]
        let yesterday30dUser = await yesterday["30 day active users (max)"]
        let todayAvgSession = await getSeconds(today["Average session duration (max)"])
        let yesterdayAvgSession = await getSeconds(yesterday["Average session duration (max)"])

        await wait(20000)

        // create slack post
        let slackMessage =
            `*📆 Daily Update:*
    
>>> *👱‍♀ Facebook Likes:*
${today["Facebook Likes"]}
(goal: ${goal["Facebook Likes"]}, ${goal["up to"]})
    
*👱‍♀ Twitter Follower:*
${today["Twitter Follower"]}
(goal: ${goal["Twitter Follower"]}, ${goal["up to"]})
    
*✉️ Mailchimp Subscriber:*
${today["Mailchimp Subscriber"]}
(goal: ${goal["Mailchimp Subscriber"]}, ${goal["up to"]})
    
*🔍 30 day active users (max)*
${today30dUser>yesterday30dUser?"📈 ":today30dUser<yesterday30dUser?"📉 ":""}${
today30dUser==yesterday30dUser?`still ${yesterday30dUser}`:
(((today30dUser / yesterday30dUser) - 1) * 100).toFixed(1) > 0 ? 
"+" + (((today30dUser / yesterday30dUser) - 1) * 100).toFixed(1) + "%" : 
(((today30dUser / yesterday30dUser) - 1) * 100).toFixed(1) + "%"}${yesterday30dUser!=today30dUser?" from "+yesterday30dUser+" to "+today30dUser:""}
(goal: ${goal["30 day active users (max)"]}, ${goal["up to"]})
    
*🔍 Average session duration (max)*
${todayAvgSession>yesterdayAvgSession?"📈 ":todayAvgSession<yesterdayAvgSession?"📉 ":""}${
todayAvgSession==yesterdayAvgSession?`still ${yesterdayAvgSession}`:
(((todayAvgSession / yesterdayAvgSession) - 1) * 100).toFixed(1) > 0 ? 
"+" + (((todayAvgSession / yesterdayAvgSession) - 1) * 100).toFixed(1) + "%" : 
(((todayAvgSession / yesterdayAvgSession) - 1) * 100).toFixed(1) + "%"}${yesterdayAvgSession!=todayAvgSession?" from "+yesterday["Average session duration (max)"]+" to "+today["Average session duration (max)"]:""}
(goal: ${goal["Average session duration (max)"]}, ${goal["up to"]})

*🔍 Watch trending searches & no results searches & optimize filter!*
https://www.algolia.com/apps/${config.Algolia_ApplicationID}/analytics/overview/${config.Algolia_Project}

*📝 Watch the latest user user sessions & make notes!*
https://app.fullstory.com/ui/${config.fullstoryID}/segments/newThisWeek/people/0

*Learn more:*

Googlesheets://${config.GoogleSheetsKeyNumberURL}

${config.GoogleSheetsKeyNumberURL}`

        await wait(20000)

        slack.post(slackMessage)


    } catch (error) {
        console.log(error)
        slack.post(error)
    }
    // }
})

// send Slack report once a week
cron.schedule("0 6 * * MON", async function () {

    // weeklyUpdate()
    // async function weeklyUpdate() {
    try {
        // get latest data from google sheets
        let today = {
            "Facebook Likes": parseInt(await GoogleSheets.getCell("overview", "C26")),
            "Twitter Follower": parseInt(await GoogleSheets.getCell("overview", "D26")),
            "Mailchimp Subscriber": parseInt(await GoogleSheets.getCell("overview", "E26")),
            "30 day active users (max)": parseInt(await GoogleSheets.getCell("overview", "C47")),
            "Average session duration (max)": await GoogleSheets.getCell("overview", "D47"),
        }

        let lastWeek = {
            "30 day active users (max)": parseInt(await GoogleSheets.getCell("overview", "C50")),
            "Average session duration (max)": await GoogleSheets.getCell("overview", "D50"),
        }

        let goal = {
            "up to": await GoogleSheets.getCell("overview", "B25"),
            "Facebook Likes": parseInt(await GoogleSheets.getCell("overview", "C25")),
            "Twitter Follower": parseInt(await GoogleSheets.getCell("overview", "D25")),
            "Mailchimp Subscriber": parseInt(await GoogleSheets.getCell("overview", "E25")),
            "30 day active users (max)": parseInt(await GoogleSheets.getCell("overview", "C46")),
            "Average session duration (max)": await GoogleSheets.getCell("overview", "D46"),
        }

        let today30dUser = await today["30 day active users (max)"]
        let lastWeek30dUser = await lastWeek["30 day active users (max)"]
        let todayAvgSession = await getSeconds(today["Average session duration (max)"])
        let lastWeekAvgSession = await getSeconds(lastWeek["Average session duration (max)"])

        await wait(20000)

        // create slack post
        let slackMessage =
            `*📆 Weekly Update:*
    
>>> *👱‍♀ Facebook Likes:*
${today["Facebook Likes"]}
(goal: ${goal["Facebook Likes"]}, ${goal["up to"]})
    
*👱‍♀ Twitter Follower:*
${today["Twitter Follower"]}
(goal: ${goal["Twitter Follower"]}, ${goal["up to"]})
    
*✉️ Mailchimp Subscriber:*
${today["Mailchimp Subscriber"]}
(goal: ${goal["Mailchimp Subscriber"]}, ${goal["up to"]})
    
*🔍 30 day active users (max)*
${today30dUser>lastWeek30dUser?"📈 ":today30dUser<lastWeek30dUser?"📉 ":""}${
today30dUser==lastWeek30dUser?`still ${lastWeek30dUser}`:
(((today30dUser / lastWeek30dUser) - 1) * 100).toFixed(1) > 0 ? 
"+" + (((today30dUser / lastWeek30dUser) - 1) * 100).toFixed(1) + "%" : 
(((today30dUser / lastWeek30dUser) - 1) * 100).toFixed(1) + "%"}${lastWeek30dUser!=today30dUser?" from "+lastWeek30dUser+" to "+today30dUser:""}
(goal: ${goal["30 day active users (max)"]}, ${goal["up to"]})
    
*🔍 Average session duration (max)*
${todayAvgSession>lastWeekAvgSession?"📈 ":todayAvgSession<lastWeekAvgSession?"📉 ":""}${
todayAvgSession==lastWeekAvgSession?`still ${lastWeekAvgSession}`:
(((todayAvgSession / lastWeekAvgSession) - 1) * 100).toFixed(1) > 0 ? 
"+" + (((todayAvgSession / lastWeekAvgSession) - 1) * 100).toFixed(1) + "%" : 
(((todayAvgSession / lastWeekAvgSession) - 1) * 100).toFixed(1) + "%"}${lastWeekAvgSession!=todayAvgSession?" from "+lastWeek["Average session duration (max)"]+" to "+today["Average session duration (max)"]:""}
(goal: ${goal["Average session duration (max)"]}, ${goal["up to"]})

*🔍 Watch trending searches & no results searches & optimize filter!*
https://www.algolia.com/apps/${config.Algolia_ApplicationID}/analytics/overview/${config.Algolia_Project}

*📝 Watch the latest user user sessions & make notes!*
https://app.fullstory.com/ui/${config.fullstoryID}/segments/newThisWeek/people/0

*Learn more:*

Googlesheets://${config.GoogleSheetsKeyNumberURL}

${config.GoogleSheetsKeyNumberURL}`

        await wait(20000)

        slack.post(slackMessage)


    } catch (error) {
        console.log(error)
        slack.post(error)
    }
    // }
})

// reminder to update financial plan of the last month
cron.schedule("0 7 1 * *", async function () {
    slack.post(`⚠️ Remember to update costs, income, Facebook likes, Twitter follower & Mailchimp subscriber for the past month, in:\n\nGooglesheets://${config.GoogleSheetsKeyNumberURL}\n\n${config.GoogleSheetsKeyNumberURL}`)

})

// send Slack report once a month
cron.schedule("0 19 1 * *", async function () {
    // lastMonth()
    // async function lastMonth() {
    try {


        // get latest data from google sheets
        let today = {
            "Facebook Likes": parseInt(await GoogleSheets.getCell("overview", "C26")),
            "Twitter Follower": parseInt(await GoogleSheets.getCell("overview", "D26")),
            "Mailchimp Subscriber": parseInt(await GoogleSheets.getCell("overview", "E26")),
            "30 day active users (max)": parseInt(await GoogleSheets.getCell("overview", "C47")),
            "Average session duration (max)": await GoogleSheets.getCell("overview", "D47"),
        }

        let lastMonth = {
            "Facebook Likes": parseInt(await GoogleSheets.getCell("overview", "C29")),
            "Twitter Follower": parseInt(await GoogleSheets.getCell("overview", "D29")),
            "Mailchimp Subscriber": parseInt(await GoogleSheets.getCell("overview", "E29")),
            "30 day active users (max)": parseInt(await GoogleSheets.getCell("overview", "C59")),
            "Average session duration (max)": await GoogleSheets.getCell("overview", "D59"),
            "monthly income total": await GoogleSheets.getCell("overview", "I29"),
            "monthly costs total": await GoogleSheets.getCell("overview", "J29"),
            "monthly income": {
                "Amazon Affiliate": await GoogleSheets.getCell("overview", "K29"),
                "Udemy": await GoogleSheets.getCell("overview", "L29"),
                "AppStore": await GoogleSheets.getCell("overview", "M29"),
            },
            "monthly costs": {
                "Algolia": await GoogleSheets.getCell("overview", "O29"),
                "DigitalOcean": await GoogleSheets.getCell("overview", "P29"),
            }
        }

        let twoMonthsAgo = {
            "monthly income total": await GoogleSheets.getCell("overview", "I30"),
            "monthly costs total": await GoogleSheets.getCell("overview", "J30"),
            "monthly income": {
                "Amazon Affiliate": await GoogleSheets.getCell("overview", "K30"),
                "Udemy": await GoogleSheets.getCell("overview", "L30"),
                "AppStore": await GoogleSheets.getCell("overview", "M30"),
            },
            "monthly costs": {
                "Algolia": await GoogleSheets.getCell("overview", "O30"),
                "DigitalOcean": await GoogleSheets.getCell("overview", "P30"),
            }
        }

        let goal = {
            "up to": await GoogleSheets.getCell("overview", "B25"),
            "Facebook Likes": parseInt(await GoogleSheets.getCell("overview", "C25")),
            "Twitter Follower": parseInt(await GoogleSheets.getCell("overview", "D25")),
            "Mailchimp Subscriber": parseInt(await GoogleSheets.getCell("overview", "E25")),
            "30 day active users (max)": parseInt(await GoogleSheets.getCell("overview", "C46")),
            "Average session duration (max)": await GoogleSheets.getCell("overview", "D46"),
            "monthly income": await GoogleSheets.getCell("overview", "I25"),
        }

        let today30dUser = await today["30 day active users (max)"]
        let lastMonth30dUser = await lastMonth["30 day active users (max)"]
        let todayAvgSession = await getSeconds(today["Average session duration (max)"])
        let lastMonthAvgSession = await getSeconds(lastMonth["Average session duration (max)"])
        let lastIncome = parseFloat((await lastMonth["monthly income total"]).replace("$", ""))
        let lastCosts = parseFloat((await lastMonth["monthly costs total"]).replace("$", ""))
        let seclastIncome = parseFloat((await twoMonthsAgo["monthly income total"]).replace("$", ""))
        let seclastCosts = parseFloat((await twoMonthsAgo["monthly costs total"]).replace("$", ""))
        let lastProfit = parseInt(await lastIncome + await lastCosts)
        let seclastProfit = parseInt(await seclastIncome + await seclastCosts)


        let slackMessage =
            `*🗓 Monthly Update:*

>>> *👱‍♀ Facebook Likes:*
${today["Facebook Likes"]>lastMonth["Facebook Likes"]?"📈 ":today["Facebook Likes"]<lastMonth["Facebook Likes"]?"📉 ":""}${
today["Facebook Likes"]==lastMonth["Facebook Likes"]?`still ${lastMonth["Facebook Likes"]}`:
(((today["Facebook Likes"] / lastMonth["Facebook Likes"]) - 1) * 100).toFixed(1) > 0 ? 
"+" + (((today["Facebook Likes"] / lastMonth["Facebook Likes"]) - 1) * 100).toFixed(1) + "%" : 
(((today["Facebook Likes"] / lastMonth["Facebook Likes"]) - 1) * 100).toFixed(1) + "%"}${lastMonth["Facebook Likes"]!=today["Facebook Likes"]?" from "+lastMonth["Facebook Likes"]+" to "+today["Facebook Likes"]:""}
(goal: ${goal["Facebook Likes"]}, ${goal["up to"]})

*👱‍♀ Twitter Follower:*
${today["Twitter Follower"]>lastMonth["Twitter Follower"]?"📈 ":today["Twitter Follower"]<lastMonth["Twitter Follower"]?"📉 ":""}${
today["Twitter Follower"]==lastMonth["Twitter Follower"]?`still ${lastMonth["Twitter Follower"]}`:
(((today["Twitter Follower"] / lastMonth["Twitter Follower"]) - 1) * 100).toFixed(1) > 0 ? 
"+" + (((today["Twitter Follower"] / lastMonth["Twitter Follower"]) - 1) * 100).toFixed(1) + "%" : 
(((today["Twitter Follower"] / lastMonth["Twitter Follower"]) - 1) * 100).toFixed(1) + "%"}${lastMonth["Twitter Follower"]!=today["Twitter Follower"]?" from "+lastMonth["Twitter Follower"]+" to "+today["Twitter Follower"]:""}
(goal: ${goal["Twitter Follower"]}, ${goal["up to"]})

*✉️ Mailchimp Subscriber:*
${today["Mailchimp Subscriber"]>lastMonth["Mailchimp Subscriber"]?"📈 ":today["Mailchimp Subscriber"]<lastMonth["Mailchimp Subscriber"]?"📉 ":""}${
today["Mailchimp Subscriber"]==lastMonth["Mailchimp Subscriber"]?`still ${lastMonth["Mailchimp Subscriber"]}`:
(((today["Mailchimp Subscriber"] / lastMonth["Mailchimp Subscriber"]) - 1) * 100).toFixed(1) > 0 ? 
"+" + (((today["Mailchimp Subscriber"] / lastMonth["Mailchimp Subscriber"]) - 1) * 100).toFixed(1) + "%" : 
(((today["Mailchimp Subscriber"] / lastMonth["Mailchimp Subscriber"]) - 1) * 100).toFixed(1) + "%"}${lastMonth["Mailchimp Subscriber"]!=today["Mailchimp Subscriber"]?" from "+lastMonth["Mailchimp Subscriber"]+" to "+today["Mailchimp Subscriber"]:""}
(goal: ${goal["Mailchimp Subscriber"]}, ${goal["up to"]})

*🔍 30 day active users (max)*
${today30dUser>lastMonth30dUser?"📈 ":today30dUser<lastMonth30dUser?"📉 ":""}${
today30dUser==lastMonth30dUser?`still ${lastMonth30dUser}`:
(((today30dUser / lastMonth30dUser) - 1) * 100).toFixed(1) > 0 ? 
"+" + (((today30dUser / lastWeek30dUser) - 1) * 100).toFixed(1) + "%" : 
(((today30dUser / lastMonth30dUser) - 1) * 100).toFixed(1) + "%"}${lastMonth30dUser!=today30dUser?" from "+lastMonth30dUser+" to "+today30dUser:""}
(goal: ${goal["30 day active users (max)"]}, ${goal["up to"]})

*🔍 Average session duration (max)*
${todayAvgSession>lastMonthAvgSession?"📈 ":todayAvgSession<lastMonthAvgSession?"📉 ":""}${
todayAvgSession==lastMonthAvgSession?`still ${lastMonthAvgSession}`:
(((todayAvgSession / lastMonthAvgSession) - 1) * 100).toFixed(1) > 0 ? 
"+" + (((todayAvgSession / lastMonthAvgSession) - 1) * 100).toFixed(1) + "%" : 
(((todayAvgSession / lastMonthAvgSession) - 1) * 100).toFixed(1) + "%"}${lastMonthAvgSession!=todayAvgSession?" from "+lastMonth["Average session duration (max)"]+" to "+today["Average session duration (max)"]:""}
(goal: ${goal["Average session duration (max)"]}, ${goal["up to"]})

*💵 Profit*
${lastProfit>seclastProfit?"📈 ":lastProfit<seclastProfit?"📉 ":""}${
lastProfit==seclastProfit?`still ${seclastProfit}`:
(((lastProfit / seclastProfit) - 1) * 100).toFixed(1) > 0 || (seclastProfit<0 && lastProfit<0)? 
"+" + (((lastProfit / seclastProfit) - 1) * 100).toFixed(1) + "%" : 
(((lastProfit / seclastProfit) - 1) * 100).toFixed(1) + "%"}${seclastProfit!=lastProfit?" from $"+seclastProfit+" to $"+lastProfit:""}

*💵 Income*
${lastIncome>seclastIncome?"📈 ":lastIncome<seclastIncome?"📉 ":""}${
lastIncome==seclastIncome?`still ${seclastIncome}`:
(((lastIncome / seclastIncome) - 1) * 100).toFixed(1) > 0 ? 
"+" + (((lastIncome / seclastIncome) - 1) * 100).toFixed(1) + "%" : 
(((lastIncome / seclastIncome) - 1) * 100).toFixed(1) + "%"}${seclastIncome!=lastIncome?" from $"+seclastIncome+" to $"+lastIncome:""}
(goal: ${goal["monthly income"]}, ${goal["up to"]})

*💵 Costs*
${lastCosts>seclastCosts?"📈 ":lastCosts<seclastCosts?"📉 ":""}${
lastCosts==seclastCosts?`still ${seclastCosts}`:
(((lastCosts / seclastCosts) - 1) * 100).toFixed(1) > 0 ? 
"+" + (((lastCosts / seclastCosts) - 1) * 100).toFixed(1) + "%" : 
(((lastCosts / seclastCosts) - 1) * 100).toFixed(1) + "%"}${seclastCosts!=lastCosts?" from $"+seclastCosts+" to $"+lastCosts:""}

*Learn more:*

Googlesheets://${config.GoogleSheetsKeyNumberURL}

${config.GoogleSheetsKeyNumberURL}`
        await wait(20000)

        slack.post(slackMessage)



    } catch (error) {
        console.log(error)
        slack.post(error)
    }
    // }
})


// check every minute if posts are ready to publish now => if yes, publish
cron.schedule("*/1 * * * *", async function () {

    // get current time in San Francisco without seconds & ms
    let time = new Date();
    let year = time.getFullYear()
    let month = time.getMonth()
    let day = time.getDate()
    let hour = time.getHours()
    let minute = time.getMinutes()
    let timeWithoutSec = new Date(year, month, day, hour, minute)
    let timeISO = timeWithoutSec.toISOString();


    base('Social media posts').select({
        // Selecting the first 10 records in Upcoming:
        maxRecords: 10,
        view: "Today"
    }).eachPage(async function page(records, fetchNextPage) {
            // This function (`page`) will get called for each page of records.

            for (record of records) {
                // console.log('Retrieved', JSON.stringify(record, null, 2));

                // Update blog posts
                if (record.get('Update Blog post')) {
                    // define objectID
                    let objectID = record.id;

                    // define blog post
                    let blogPost = record.get('Publish Blog post') ? {
                        name: record.get('Name'),
                        slug: record.get('Blog post slug'),
                        summary: record.get('Blog post summary'),
                        previewImage: record.get('Preview Image'),
                        message: record.get('Blog post'),
                        AlgoliaGuideMessage: record.get('Blog post AlgoliaGuideMessage'),
                        AlgoliaCTAGetStarted: record.get("Blog post \"Get started\" CTA"),
                        AlgoliaTopics: record.get("Topics").split(",")
                    } : null;

                    await blog.update(
                        objectID,
                        blogPost.name,
                        blogPost.slug,
                        blogPost.summary,
                        blogPost.previewImage,
                        blogPost.message,
                        blogPost.AlgoliaGuideMessage,
                        blogPost.AlgoliaCTAGetStarted,
                        blogPost.AlgoliaTopics)

                }

                //check if date is now
                if (record.get('Date') == timeISO) {
                    // if true: check if Ready to publish (and send error via Slack if not!)
                    if (record.get('Autocheck') == "✅" && record.get('Ready to publish') == true) {
                        // define objectID
                        let objectID = record.id;
                        // define facebook post
                        let facebookPost = record.get('Publish to Facebook') ? record.get('Facebook post') : null;

                        // define twitter post
                        let twitterPost = record.get('Publish to Twitter') ? record.get('Twitter post') : null;
                        let TwitterAccounts = record.get('Publish to Twitter') ? record.get('TwitterAccounts') ? record.get('TwitterAccounts') : "SkillMe" : null;

                        // define newsletter
                        let newsletterPost = record.get('Publish to Newsletter') ? {
                            testOrPublish: record.get('Newsletter Test or Publish'),
                            name: record.get('Name'),
                            message: record.get('E-Mail Newsletter'),
                            previewImage: record.get('Preview Image'),
                            campaignType: record.get('E-Mail campaignType'),
                            settings: {
                                subject_line: record.get('E-Mail subject_line'),
                                preview_text: record.get('E-Mail preview_text')
                            }
                        } : null;

                        // define blog post
                        let blogPost = record.get('Publish Blog post') ? {
                            name: record.get('Name'),
                            slug: record.get('Blog post slug'),
                            summary: record.get('Blog post summary'),
                            previewImage: record.get('Preview Image'),
                            message: record.get('Blog post'),
                            AlgoliaGuideMessage: record.get('Blog post AlgoliaGuideMessage'),
                            AlgoliaCTAGetStarted: record.get("Blog post \"Get started\" CTA"),
                            AlgoliaTopics: record.get("Topics").split(",")
                        } : null;

                        let CodingCommunitiesPost = record.get('Publish to Coding Communities') ? record.get('Facebook post') : null;

                        // publish to channels
                        await publishPost(objectID, facebookPost, twitterPost, TwitterAccounts, newsletterPost, blogPost, CodingCommunitiesPost)

                    } else {
                        console.log(`❌ Couldn't publish "${record.get('Name')}": Ready to publish is false!`)
                        slack.post(`❌ Couldn't publish "${record.get('Name')}": Ready to publish is false!`)
                    }
                }

                await wait(5000)
            };

            // To fetch the next page of records, call `fetchNextPage`.
            // If there are more records, `page` will get called again.
            // If there are no more records, `done` will get called.
            fetchNextPage();

        },
        function done(err) {
            if (err) {
                console.error(err);
                slack.post(err)
                return;
            }
        })
})


// function: publish post
async function publishPost(objectID, facebookPost, TwitterPost, TwitterAccounts, newsletterPost, blogPost, CodingCommunitiesPost) {
    try {
        // check for Facebook, Twitter, newsletter, blogPost if content exist
        if (blogPost) {
            await blog.publish(
                objectID,
                blogPost.name,
                blogPost.slug,
                blogPost.summary,
                blogPost.previewImage,
                blogPost.message,
                blogPost.AlgoliaGuideMessage,
                blogPost.AlgoliaCTAGetStarted,
                blogPost.AlgoliaTopics)
        }
        if (newsletterPost) {
            await mailchimp.publish(
                objectID,
                newsletterPost.testOrPublish,
                newsletterPost.name,
                newsletterPost.message,
                newsletterPost.previewImage,
                newsletterPost.campaignType,
                newsletterPost.settings)
        }
        if (facebookPost) {
            await facebook.publish(
                facebookPost)
        }
        if (TwitterPost) {
            // post to twitter channels
            if (TwitterAccounts.includes("Marco")) {
                await twitter.publish(
                    objectID,
                    TwitterPost.replace(/(\sSkillMe)/g, " @heySkillMe"),
                    "Marco")
            }
            if (TwitterAccounts.includes("SkillMe")) {
                await twitter.publish(
                    objectID,
                    TwitterPost,
                    "SkillMe")
            }
        }
        if (CodingCommunitiesPost) {
            slack.post(`To Do: post in groups on https://airtable.com/tblmRffJAp8UPrGxO/viwW5IXdOsbFmGVBA:\n\n${CodingCommunitiesPost}`)
        }

    } catch (error) {
        console.log(error)
        slack.post(error)
    }
}