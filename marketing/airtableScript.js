// created by
// Marco Bartsch


const cron = require("node-cron");
const fs = require('fs')

const Airtable = require('airtable');
const config = require("../database/config.json")
var base = new Airtable({
    apiKey: config.airtable_apiKey
}).base(config.airtable_baseID);

function wait(ms) {
    return new Promise(resolve => {
        setTimeout(() => resolve(), ms);
    });
}


module.exports = {
    update: function (objectID, field, newContent) {
        base('Social media posts').update(objectID, {
            [field]: newContent
        }, function (err, record) {
            if (err) {
                console.error(err);
                return;
            }
            console.log(`Updated ${record.get('Name')} on Airtable`);
        });
    },
    backup: async function () {
        let bases = config.airtable_bases

        await bases.forEach(
            async (name) => {
                let file = []
                await base(name).select({
                    maxRecords: 100,
                    view: "Grid view"
                }).eachPage(await async function page(records, fetchNextPage) {
                        for (record of records) {
                            file.push(record._rawJson)
                        }

                        // To fetch the next page of records, call `fetchNextPage`.
                        // If there are more records, `page` will get called again.
                        // If there are no more records, `done` will get called.
                        await wait(500)
                        fetchNextPage();
                    },
                    async function done(err) {
                        await fs.writeFile(`./marketing/backup/${name}.json`, JSON.stringify(file, null, 2));
                        file = []
                        console.log(`Backup of ${name} complete`)
                        if (err) {
                            console.error(err);
                            return;
                        }
                    })

            })
    }
}

// module.exports.backup()
// console.log("Scheduled backup...")

// // once a day create new entries I otherwise create manual
// cron.schedule("0 0 * * *", async function () {
//     module.exports.backup()
// })