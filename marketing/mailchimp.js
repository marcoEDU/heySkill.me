// created by
// Marco Bartsch

// https://www.npmjs.com/package/mailchimp-api-v3

const fromName = "Marco, founder at SkillMe - Your guide to learn to code."
const replyTo = "marco@heyskill.me"

const config = require("../database/config.json")
const Mailchimp = require('mailchimp-api-v3')
const markdownToHTML = require('./markdownToHTML')
const fs = require('fs');
const slack = require("../database/slack.js");
let mailchimp = new Mailchimp(config.mailchimp_APIkey);
const airtable = require("./airtableScript.js")
var base64 = require('node-base64-image');
var path = require('path');

function wait(ms) {
    return new Promise(resolve => {
        setTimeout(() => resolve(), ms);
    });
}

module.exports = {
    //TO DO 📌 - upload images to mailchimp first -> can't make promises work

    uploadImage: function (imageURL, callback) {
        // get filename
        let filename = imageURL.substr(imageURL.lastIndexOf("/") + 1)

        base64.encode(imageURL, {
            string: true
        }, function (err, file) {
            if (err) {
                callback(err)
            } else {
                // https://developer.mailchimp.com/documentation/mailchimp/reference/file-manager/files/
                mailchimp.post(`/file-manager/files`, {
                    name: fileName,
                    file_data: file
                }, function (err, data) {
                    if (err) {
                        callback(err)
                    } else {
                        callback(null, data.full_size_url)
                    }
                })
            }
        })
    },
    // function: publish email to newsletter
    publish: async function (objectID, testOrPublish, name, message, previewImage, campaignType, settings) {
            try {
                // message: string (markdown or plaintext)
                // campaignType: string
                // settings: array



                //TO DO 📌 - upload images to mailchimp first -> can't make promises work

                // let images = [previewImage]

                // create array with highlighted changes
                let changesArray = []
                let changes = message.match(/(- \[(.)+\n)/g)
                if (changes) {
                    for (change of changes) {
                        let imageURL = change.match(/(\{(.)+\})/)[0].replace(/[\{\}]/g, "")
                        changesArray.push({
                            name: change.match(/(\[(.)+\])/)[0].replace(/[\[\]]/g, ""),
                            link: change.match(/(\((.)+\))/)[0].replace(/[\(\)]/g, ""),
                            imageURL: imageURL
                        })
                        //TO DO 📌 - upload images to mailchimp first -> can't make promises work

                        // images.push(imageURL)
                    }
                }

                // create fields based on markdown message
                let blogpostURL = message.match(/(\(.+\))/) ? (message.match(/(\(.+\))/)[0]).replace(/[\(\)]/g, "") : null;
                let headline = message.match(/(# (.)+\n)/) ? (((message.match(/(# (.)+\n)/)[0])).replace("# ", "")).replace(/\n/g, "") : null;
                let backgroundImageURL = previewImage
                let intro = message.match(/(## (.)+\n)/g) ? (((message.match(/(## (.)+\n)/g)[0])).replace("## ", "")).replace(/\n/g, "") : null;
                let whatsNewSlogan = message.match(/(## (.)+\n)/g) ? (((message.match(/(## (.)+\n)/g)[1])).replace("## ", "")).replace(/\n/g, "") : null;
                let lastSentence = message.match(/(## (.)+)/g) ? (((message.match(/(## (.)+)/g)[2])).replace("## ", "")) : null;

                //TO DO start 📌
                // get trending searches from Airtable or Algolia directly
                //TO DO end 📌
                let trendingSearches = [{
                    topic: "Python",
                    URL: "https://heyskill.me/search?q=Python"
                }, {
                    topic: "web development",
                    URL: "https://heyskill.me/search?q=web%20development"
                }, {
                    topic: "blockchain",
                    URL: "https://heyskill.me/search?q=blockchain"
                }]


                //TO DO 📌 - upload images to mailchimp first -> can't make promises work

                // function uploadALl(images) {
                //     return Promise.all(images.map(imageURL => module.exports.uploadImage(imageURL))).then(function (newImageURLs) {
                //         console.log(newImageURLs)
                //         return newImageURLs
                //     })
                // }


                // images = await uploadALl(images)
                // console.log(images)


                // decide what segment to choose, based on campaignType
                let segmentIDs = {
                    productAnnouncements: config.mailchimp_SegmentID_productAnnouncements,
                    recommendations: config.mailchimp_SegmentID_recommendations,
                    callForParticipations: config.mailchimp_SegmentID_recommendations,
                    behindTheScenes: config.mailchimp_SegmentID_behindTheScenes,
                }
                let segmentID = segmentIDs[campaignType]

                // load HTML template, based on campaignType
                fs.readFile(path.join(__dirname, `/templates/newsletter_${campaignType}.html`), 'utf8', async function (err, html) {
                    if (err) {
                        throw err;
                    }

                    // convert markdown message to html
                    if (campaignType == "productAnnouncements") {
                        let changesHTML = "";
                        let integratedChanges = [];

                        if (changes) {
                            for (change = 0; change < (changesArray.length) - 1; change++) {
                                // check if change was already listed / used
                                if (!integratedChanges.includes(change)) {
                                    // build html block with new changes
                                    if (change == 0) {
                                        changesHTML = changesHTML + `
                            <!--[if (gte mso 9)|(IE)]>
                            <table align="center" border="0" cellspacing="0" cellpadding="0" width="600" style="width:600px;">
                            <tr>
                            <td align="center" valign="top" width="200" style="width:200px;">
                            <![endif]-->`
                                    }
                                    // check if object in first row and object under that one exist, if yes create block with both, else only with one
                                    if (changesArray[parseInt(change) + 3]) {
                                        change = parseInt(change)
                                        integratedChanges.push(change);
                                        integratedChanges.push(change + 3);

                                        changesHTML = changesHTML + `
                            <table align="left" border="0" cellpadding="0" cellspacing="0" width="200" class="columnWrapper">
                                <tbody><tr>
                                    <td valign="top" class="columnContainer">
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnCaptionBlock">
                                            <tbody class="mcnCaptionBlockOuter">
                                                <tr>
                                                    <td class="mcnCaptionBlockInner" valign="top" style="padding:9px;">
                                                        <table align="left" border="0" cellpadding="0" cellspacing="0" class="mcnCaptionBottomContent">
                                                            <tbody><tr>
                                                                    <td class="mcnCaptionBottomImageContent" align="center" valign="top" style="padding:0 9px 9px 9px;">
                                                                        <a href="${changesArray[change].link}" title="" class="" target="_blank">
                                                                            <img alt="" src="${changesArray[change].imageURL}" width="164" style="max-width:400px;" class="mcnImage">
                                                                        </a>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="mcnTextContent" valign="top" style="padding:0 9px 0 9px;" width="164">
                                                                        <span style="font-family:lucida sans unicode,lucida grande,sans-serif"><strong>${changesArray[change].name}</strong><br>
                                                                            <a href="${changesArray[change].link}" target="_blank">Learn more »</a></span>
                                                                    </td>
                                                                </tr>
                                                            </tbody></table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnCaptionBlock">
                                            <tbody class="mcnCaptionBlockOuter">
                                                <tr>
                                                    <td class="mcnCaptionBlockInner" valign="top" style="padding:9px;">
                                                        <table align="left" border="0" cellpadding="0" cellspacing="0" class="mcnCaptionBottomContent">
                                                            <tbody><tr>
                                                                    <td class="mcnCaptionBottomImageContent" align="center" valign="top" style="padding:0 9px 9px 9px;">
                                                                        <a href="${changesArray[parseInt(change) + 3].link}" title="" class="" target="_blank">
                                                                            <img alt="" src="${changesArray[parseInt(change) + 3].imageURL}" width="164" style="max-width:400px;" class="mcnImage">
                                                                        </a>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="mcnTextContent" valign="top" style="padding:0 9px 0 9px;" width="164">
                                                                        <span style="font-family:lucida sans unicode,lucida grande,sans-serif"><strong>${changesArray[parseInt(parseInt(change) + 3)].name}</strong><br>
                                                                            <a href="${changesArray[parseInt(change) + 3].link}" target="_blank">Learn more »</a></span>
                                                                    </td>
                                                                </tr>
                                                            </tbody></table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tbody></table>`
                                    } // make sure to stop after 3rd loop
                                    else if (change < (changesArray.length) - 3) {
                                        change = parseInt(change)
                                        integratedChanges.push(change);

                                        changesHTML = changesHTML +
                                            `<table align="left" border="0" cellpadding="0" cellspacing="0" width="200" class="columnWrapper">
                                <tbody><tr>
                                    <td valign="top" class="columnContainer">
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnCaptionBlock">
                                            <tbody class="mcnCaptionBlockOuter">
                                                <tr>
                                                    <td class="mcnCaptionBlockInner" valign="top" style="padding:9px;">
                                                        <table align="left" border="0" cellpadding="0" cellspacing="0" class="mcnCaptionBottomContent">
                                                            <tbody><tr>
                                                                    <td class="mcnCaptionBottomImageContent" align="center" valign="top" style="padding:0 9px 9px 9px;">
                                                                        <img alt="" src="${changesArray[change].imageURL}" width="164" style="max-width:400px;" class="mcnImage">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="mcnTextContent" valign="top" style="padding:0 9px 0 9px;" width="164">
                                                                        <span style="font-family:lucida sans unicode,lucida grande,sans-serif"><strong>${changesArray[change].name}</strong><br>
                                                                            <a href="${changesArray[change].link}" target="_blank">Learn more »</a></span>
                                                                    </td>
                                                                </tr>
                                                            </tbody></table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tbody></table>`
                                    }


                                    // if not last block => add middle comment
                                    if (change < changesArray.length) {
                                        changesHTML = changesHTML + `
                                    <!--[if (gte mso 9)|(IE)]>
                                    </td>
                                    <td align="center" valign="top" width="200" style="width:200px;">
                                    <![endif]-->`
                                    } else {
                                        // if last block => add closing comment
                                        changesHTML = changesHTML + `
                            <!--[if (gte mso 9)|(IE)]>
                            </td>
                            </tr>
                            </table>
                            <![endif]-->`
                                        break
                                    }
                                }
                            }
                        }

                        for (number in trendingSearches) {
                            html = html.replace(`{{trendingSearches[${number}].topic}}`, trendingSearches[number].topic);
                            html = html.replace(`{{trendingSearches[${number}].URL}}`, trendingSearches[number].URL);
                        }

                    } else {
                        // make sure headline is not missing
                        let htmlMessage = await markdownToHTML.convert(message)
                        html = html.replace("{{message}}", htmlMessage)
                    }

                    // replace template fields with content
                    html = html.replace("{{backgroundImageURL}}", backgroundImageURL);
                    html = html.replace(/(\{\{blogpostURL\}\})/g, blogpostURL);
                    html = html.replace("{{headline}}", headline);
                    html = html.replace("{{intro}}", intro);
                    html = html.replace("{{whatsNewSlogan}}", whatsNewSlogan);
                    html = html.replace("{{lastSentence}}", lastSentence);
                    try {
                        html = html.replace("{{changesHTML}}", changesHTML);
                    } catch (error) {}

                    // create new campaign based on html
                    // http://developer.mailchimp.com/documentation/mailchimp/reference/campaigns/
                    await mailchimp.post('/campaigns', {
                        type: 'regular',
                        recipients: {
                            list_id: config.mailchimp_listID,
                            segment_opts: {
                                saved_segment_id: segmentID
                            }
                        },
                        settings: {
                            subject_line: settings.subject_line,
                            preview_text: settings.preview_text,
                            title: name,
                            from_name: fromName,
                            reply_to: replyTo,
                            auto_tweet: false,
                        }
                    }, async function (err, result) {
                        if (err) {
                            console.log(err)
                        }
                        // get ID from newly created campaign
                        let campaignID = (await result).id

                        // edit campaign: add message
                        await module.exports.updateCampaignHTML(campaignID, html)

                        // send campaign
                        if (testOrPublish == "Publish") {
                            module.exports.sendCampaign(objectID, campaignID)
                        } else {
                            module.exports.sendCampaignTest(campaignID)
                        }

                        console.log(`✅ Sent newsletter @${campaignType}: "${name}"`)
                        slack.post(`✅ Sent newsletter @${campaignType}: "${name}"`)
                    })
                })
            } catch (error) {
                console.log(error)
            }
        },
        getTemplates: function () {
            mailchimp.get({
                path: '/templates'
            }, function (err, result) {
                console.log("Templates:")
                console.log(JSON.stringify(result, null, 2))
            })
        },
        getTemplateContent: function (template_id) {
            mailchimp.get({
                path: `/templates/${template_id}/default-content`
            }, function (err, result) {
                console.log(result)
            })
        },
        getCampaigns: function () {
            mailchimp.get({
                path: '/campaigns'
            }, function (err, result) {
                console.log("Campaigns:")
                console.log(JSON.stringify(result, null, 2))
            })
        },
        getLists: function () {
            mailchimp.get({
                path: '/lists'
            }, function (err, result) {
                console.log("Lists:")
                for (list of result.lists) {
                    // console.log(list)
                    console.log(`${list.name}, ID: ${list.id}, ${list.stats.member_count} members`)
                }
            })
        },
        getSegments: function (list_id) {
            mailchimp.get({
                path: `/lists/${list_id}/segments`
            }, function (err, result) {
                console.log("Segments:")
                for (segment of result.segments) {
                    // console.log(list)
                    console.log(`${segment.name}, ID: ${segment.id}, ${segment.member_count} members`)
                }
            })
        },
        updateCampaignHTML: async function (campaign_id, htmlMessage) {
                // http://developer.mailchimp.com/documentation/mailchimp/reference/campaigns/content/
                await mailchimp.put(`/campaigns/${campaign_id}/content`, {
                    html: htmlMessage
                })
            },
            sendCampaignTest: async function (campaign_id) {
                    // http://developer.mailchimp.com/documentation/mailchimp/reference/campaigns/#action-post_campaigns_campaign_id_actions_test
                    await mailchimp.post(`/campaigns/${campaign_id}/actions/test`, {
                        test_emails: ["contact@marcobartsch.me"],
                        send_type: "html"
                    })
                },
                sendCampaign: async function (objectID, campaign_id) {
                    // http://developer.mailchimp.com/documentation/mailchimp/reference/campaigns/#action-post_campaigns_campaign_id_actions_send
                    await mailchimp.post(`/campaigns/${campaign_id}/actions/send`)
                    airtable.update(objectID, "Newsletter URL", `https://us11.admin.mailchimp.com/reports/summary?id=${campaign_id}`)
                },
}