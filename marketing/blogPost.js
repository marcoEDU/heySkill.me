// created by
// Marco Bartsch

// https://github.com/Medium/medium-sdk-nodejs
var path = require('path');
const config = require(path.join(__dirname, "../database/config.json"))
const medium = require('medium-sdk')
const markdownToHTML = require('./markdownToHTML')
const algolia = require(path.join(__dirname, "../database/algolia"))
const fs = require('fs');
const slack = require(path.join(__dirname, "../database/slack.js"));
const airtable = require("./airtableScript.js")

function wait(ms) {
    return new Promise(resolve => {
        setTimeout(() => resolve(), ms);
    });
}

var client = new medium.MediumClient({
    clientId: config.medium_ClientID,
    clientSecret: config.medium_ClientSecret
})
client.setAccessToken(config.medium_accessToken)

var redirectURL = 'https://github.com/marcoEDU/SkillMe';

var url = client.getAuthorizationUrl('secretState', redirectURL, [
    medium.Scope.BASIC_PROFILE, medium.Scope.PUBLISH_POST
])



module.exports = {
    update: async function (objectID, name, slug, summary, previewImage, message, AlgoliaGuideMessage, AlgoliaCTAGetStarted, AlgoliaTopics) {
            // load & update & save HTML template
            await fs.readFile(path.join(__dirname, `/templates/blogPostTemplate.html`), 'utf8', async function (err, html) {
                if (err) {
                    throw err;
                }
                // convert markdown message to html
                let htmlMessage = await markdownToHTML.convert(message)

                html = html.replace(/{{name}}/g, name)
                html = html.replace(/{{slug}}/g, slug)
                html = html.replace(/{{summary}}/g, summary)
                html = html.replace("{{previewImage}}", previewImage)
                html = html.replace("{{message}}", htmlMessage)

                await fs.writeFile(path.join(__dirname, `../docs/blog/${slug}.html`), html)

                // git push only updated file => commit & push
                // https://www.npmjs.com/package/simple-git
                await require('simple-git')()
                    .pull('origin', 'master')
                    .add(path.join(__dirname, `../docs/blog/${slug}.html`))
                    .commit(`Updated blog post: "${name}"`, path.join(__dirname, `../docs/blog/${slug}.html`))
                    .push('origin', 'master')


                airtable.update(objectID, "Update Blog post", null)
                console.log(
                    `✅ Updated blog post: "${name}" - https://heyskill.me/blog/${slug}\n⚠️ Startpage entry on SkillMe not updated. If updated needed: go to https://www.algolia.com/apps/IONWOKWJBK/explorer/browse/specialHints\n⚠️ Mediums API doesn't support 'Update' a post. Update manual if needed! https://medium.com/me/stories/public`)
                slack.post(
                    `✅ Updated blog post: "${name}" - https://heyskill.me/blog/${slug}\n⚠️ Startpage entry on SkillMe not updated. If updated needed: go to https://www.algolia.com/apps/IONWOKWJBK/explorer/browse/specialHints\n⚠️ Mediums API doesn't support 'Update' a post. Update manual if needed! https://medium.com/me/stories/public`)
            })
        },

        // function: publish post to website / blog post & start page & medium
        publish: async function (objectID, name, slug, summary, previewImage, message, AlgoliaGuideMessage, AlgoliaCTAGetStarted, AlgoliaTopics) {
            try {
                // name: string (plaintext)
                // message: string (markdown)

                // publish to website
                //////////////////////

                // load & update & save HTML template
                await fs.readFile(path.join(__dirname, `/templates/blogPostTemplate.html`), 'utf8', async function (err, html) {
                    if (err) {
                        throw err;
                    }
                    // convert markdown message to html
                    let htmlMessage = await markdownToHTML.convert(message)

                    html = html.replace(/{{name}}/g, name)
                    html = html.replace(/{{slug}}/g, slug)
                    html = html.replace(/{{summary}}/g, summary)
                    html = html.replace("{{previewImage}}", previewImage)
                    html = html.replace("{{message}}", htmlMessage)

                    await fs.writeFile(path.join(__dirname, `../docs/blog/${slug}.html`), html);

                    // git push only new file => commit & push
                    // https://www.npmjs.com/package/simple-git
                    await require('simple-git')()
                        .pull('origin', 'master')
                        .add(path.join(__dirname, `../docs/blog/${slug}.html`))
                        .commit(`Added blog post: "${name}"`, path.join(__dirname, `../docs/blog/${slug}.html`))
                        .push('origin', 'master')

                    await wait(5000)

                    airtable.update(objectID, "Blogpost SkillMe URL", `https://heyskill.me/blog/${slug}`)

                    // highlight on startpage => add to Algolia
                    /////////////////////
                    await algolia.addToSpecialHints({
                        name: name,
                        slug: slug,
                        summary: summary,
                        AlgoliaGuideMessage: AlgoliaGuideMessage,
                        AlgoliaCTAGetStarted: AlgoliaCTAGetStarted,
                        previewImage: previewImage,
                        AlgoliaTopics: AlgoliaTopics
                    })


                    // publish to medium
                    /////////////////////

                    // make sure medium post is clean from new tab tags
                    message = message.replace(/({newTab})/g, "")
                    // make sure medium post is clean from explainations
                    message = message.replace(/\(\?\[.*\]\)/g, "")

                    // add link at the end to start learning path


                    // remove links to the same page
                    let samepagelinks = message.match(/\[.*\]\(.*#.*\)/g)
                    if (samepagelinks) {
                        for (link of samepagelinks) {
                            let name = link.match(/\[.*\]/)[0].replace(/[\[\]]+/g, "")
                            message = message.replace(link, name)
                        }
                    }

                    client.createPostInPublication({
                        publicationId: "d9016e301877",
                        title: name,
                        contentFormat: medium.PostContentFormat.MARKDOWN,
                        content: message,
                        tags: ["software development", "coding", "beginner guide"],
                        publishStatus: medium.PostPublishStatus.PUBLIC
                    }, function (err, post) {
                        if (err) {
                            console.log(err)
                            slack.post(err)
                        }
                        console.log(`✅ Published on Medium @SkillMe: "${name}" - ${post.url}\nRemember to check manual & update post!`)
                        slack.post(`✅ Published on Medium @SkillMe: "${name}" - ${post.url}\nRemember to check manual & update post!`)
                        airtable.update(objectID, "Blogpost Medium URL", post.url)
                    })
                })
            } catch (error) {
                console.log(error)
            }
        }
}