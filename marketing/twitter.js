// created by
// Marco Bartsch

const config = require("../database/config.json")
const Twitter = require('twitter');
const slack = require("../database/slack.js");
const airtable = require("./airtableScript.js")
var client

module.exports = {
    // function: publish post to Twitter
    // https://developer.twitter.com/en/docs.html
    publish: function (objectID, message, where) {
        // message: string (plaintext)

        // define what account to post on
        client = new Twitter({
            consumer_key: where == "Marco" ? config.twitter_marco_consumerKey : config.twitter_skillme_consumerKey,
            consumer_secret: where == "Marco" ? config.twitter_marco_consumerSecret : config.twitter_skillme_consumerSecret,
            access_token_key: where == "Marco" ? config.twitter_marco_tokenKey : config.twitter_skillme_tokenKey,
            access_token_secret: where == "Marco" ? config.twitter_marco_tokenSecret : config.twitter_skillme_tokenSecret
        });

        client.post('statuses/update', {
            status: message
        }, function (error, tweet, response) {
            if (error) throw error;
            console.log(`✅ Published on Twitter @SkillMe: "${message}" - https://twitter.com/SkillMe/status/${tweet.id_str}`)
            slack.post(`✅ Published on Twitter @SkillMe: "${message}" - https://twitter.com/SkillMe/status/${tweet.id_str}`)
            airtable.update(objectID, "Tweet URL", `https://twitter.com/SkillMe/status/${tweet.id_str}`)
        });
    }
}