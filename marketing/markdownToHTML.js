// created by
// Marco Bartsch

// https://github.com/showdownjs/showdown


module.exports = {
    convert: async function (markdown) {
        if (markdown.length > 10) {
            // remove headline from html message
            markdown = markdown.replace(/\# .*\n\n/, "")


            // make sure image tags are always in separate line
            markdown = markdown.replace(/( \!\[)/g, "\n\n![")
            markdown = markdown.replace(/(.\!\[)/g, "\n\n![")
            markdown = markdown.replace(/(.\n\!\[)/g, "\n\n![")
            markdown = markdown.replace(/\n/g, "<br>")


            var showdown = require('showdown'),
                converter = new showdown.Converter(),
                text = markdown,
                html = converter.makeHtml(text);

            // make links open in new tab
            html = html.replace(/({newTab}">)/g, "\" target=\"_blank\">")

            // convert youtube links to embedded video

            // get youtubeVideos
            let youtubeVideos = html.match(/https\:\/\/www.youtube.com\/watch\?v=[A-Za-z0-9\-]+/g)

            if (youtubeVideos) {
                for (url of youtubeVideos) {
                    let id = url.match(/v=(.*)/)[1]
                    let embeddedVideo = `<br><br><iframe width="100%" height="350" 
                 src="https://www.youtube.com/embed/${id}" 
                 frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe><br><br>`
                    html = html.replace(url, embeddedVideo)
                }
            }

            // get youtubePlaylists
            let youtubePlaylists = html.match(/https\:\/\/www.youtube.com\/playlist\?list=[A-Za-z0-9\-]+/g)

            if (youtubePlaylists) {
                for (url of youtubePlaylists) {
                    let id = url.match(/list=(.*)/)[1]
                    let embeddedVideo = `<br><br><iframe width="100%" height="350" 
                src="https://www.youtube.com/embed/videoseries?list=${id}" 
                frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe><br><br>`
                    html = html.replace(url, embeddedVideo)
                }
            }

            // replace words with explaination with popup explaination
            let explainations = html.match(/\?\[.*?\]/g);

            if (explainations) {
                explainations = explainations.toString().replace(/\?\[/g, "[")
                explainations = explainations.replace(/\],\[/g, "]],[[").split("],[")

                for (explaination of explainations) {
                    explaination = JSON.parse(explaination)
                    let button = `<div class="openexplaination" onclick='showexplaination("${explaination[0]}","${explaination[1]}")' onmouseover='showexplaination("${explaination[0]}","${explaination[1]}")' onmouseout="hideexplaination()">?</div>`
                    html = html.replace(`(?["${explaination[0]}","${explaination[1]}"])`, button)
                }
            }

            // replace <p> with <div>
            html = html.replace(/(\<p\>)/g, "<div>")
            html = html.replace(/(\<\/p\>)/g, "</div>")

            if (html.length > 10) {
                return html
            }
        }
    }
}