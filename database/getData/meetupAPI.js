// created by
// Marco Bartsch

//API methods: https://github.com/jkutianski/meetup-api/wiki/API-Methods-List

//API rate limit is 200 requests per hour and per request 200 results
// 1 city = 200 groups = 200x saveGroupEvents => 200 requests/ 1 city per hour
// => max 24 cities if daily update

const config = require("../config.json");
const fs = require("fs");
const correctResult = require("./correctResult.js");
const meetup = require('meetup-api')({
    key: config.Meetup_APIkey
});
const slack = require("../slack.js");

function wait(ms) {
    return new Promise(resolve => {
        setTimeout(() => resolve(), ms);
    });
}

module.exports = {
    copyAllFiles: async function (from, to) {
        let cities = JSON.parse(fs.readFileSync(`../data/_unfinished/meetup/cities.json`));

        for (city of cities.result) {
            try {
                fs.createReadStream(`${from}meetup_groups_${city.country}_${city.city}.json`).pipe(fs.createWriteStream(`${to}meetup_groups_${city.country}_${city.city}.json`));
                fs.createReadStream(`${from}meetup_events_${city.country}_${city.city}.json`).pipe(fs.createWriteStream(`${to}meetup_events_${city.country}_${city.city}.json`));
            } catch (error) {
                console.log(error);
                slack.post(error);
            }
        }
    },


    saveAllUpcomingEvents: async function () {
        let cities = JSON.parse(fs.readFileSync(`../data/_unfinished/meetup/cities.json`));
        //save all the latest groups & events
        for (city of cities.result) {
            await module.exports.saveCityEvents(city.city, city.country, city.state)
        }
    },

    saveCategories: async function () {
        //https://github.com/jkutianski/meetup-api/wiki/API-Methods-List#categories

        let parameters = {}

        meetup.getCategories(parameters, function (err, resp) {
            if (err) {
                console.log("There was an error...")
            } else {
                fs.writeFileSync(`../data/_unfinished/meetup/categories.json`, JSON.stringify(resp, null, 2));
                console.log("Saved meetup categories!");
            }
        });
    },

    saveCities: async function (countryCode) {
        //https://github.com/jkutianski/meetup-api/wiki/API-Methods-List#cities

        let parameters = {
            country: countryCode
        }

        meetup.getCities(parameters, function (err, resp) {
            if (err) {
                console.log("There was an error...")
            } else {
                fs.writeFileSync(`../data/_unfinished/meetup/cities_${parameters.country}.json`, JSON.stringify(resp, null, 2));
                console.log(`Saved cities from ${parameters.country}!`);
            }
        });
    },

    saveGroups: async function (city, country, state) {
        //https://github.com/jkutianski/meetup-api/wiki/API-Methods-List#groups

        let groups = {
            result: []
        }

        //save the groups for all following topics
        let topics = [
            "softwaredev",
            "robotics",
            "arduino",
            "robotics-programming",
            "computer-programming",
            "learn-to-code",
            "learn-coding",
            "women-who-code",
            "coding-for-beginners",
            "learning-to-code",
            "coding-dojos",
            "code",
            "software-engineering",
            "software-architecture",
            "women-software-developers",
            "web-development",
            "app-development",
            "game-development"
        ]

        //save groups for all topics
        for (topic of topics) {
            //make 5 requests => up to 1000 groups
            for (var i = 0; i < 3; i++) {
                if (state) {
                    var parameters = {
                        order: "members",
                        topic: topic,
                        country: country,
                        city: city,
                        state: state,
                        page: 200,
                        offset: i
                    }
                } else {
                    var parameters = {
                        order: "members",
                        topic: topic,
                        country: country,
                        city: city,
                        page: 200,
                        offset: i
                    }
                }

                console.log("wait 19 seconds for rate limit...")
                await wait(19000)
                meetup.getGroups(parameters, async function (err, resp) {
                    if (err) {
                        console.log("There was an error...")
                    } else {
                        //save groups
                        for (group of resp.results) {
                            //check if group already exists in file, if not, add it
                            if (groups.result.some(existinggroup => {
                                    return existinggroup.meetupID == group.id
                                })) {} else {
                                //check if group has more then 9 members
                                if (group.members > 9) {
                                    group = {
                                        name: group.name,
                                        meetupID: group.id,
                                        meetupurlname: group.urlname,
                                        slug: group.urlname,
                                        typeName: "group",
                                        description: group.description,
                                        image: group.group_photo ? (group.group_photo).photo_link : null,
                                        descriptionSourceName: "Meetup",
                                        UTCoffset: group.utc_offset,
                                        city: group.city,
                                        country: group.country,
                                        lon: group.lon,
                                        lat: group.lat,
                                        state: group.state ? group.state : null,
                                        members: group.members,
                                        category: "Learning",
                                        IFcontentSource1: "Meetup",
                                        averageRating: group.rating > 0 ? group.rating : null,
                                        contenturl: group.link,
                                        releaseDate: group.created,
                                        meetuptopics: group.topics.map(topic => topic.name)
                                    }
                                    group = await correctResult.correct(group)
                                    group = await correctResult.deleteNullFields(group)

                                    groups.result.push(group)
                                }
                            }
                        }
                        fs.writeFileSync(`../data/_unfinished/meetup/meetup_groups_${parameters.country}_${parameters.city}.json`, JSON.stringify(groups, null, 2));
                        console.log(`Saved groups from ${parameters.country},${parameters.city}!`);
                    }
                });
            }
        }
    },

    saveGroupEvents: async function (group_id) {
        //https://github.com/jkutianski/meetup-api/wiki/API-Methods-List#events

        let parameters = {
            group_id: group_id
        }

        console.log("wait 19 seconds for rate limit...")
        await wait(19000)
        meetup.getEvents(parameters, function (err, resp) {
            if (err) {
                console.log("There was an error...")
            } else {
                fs.writeFileSync(`../data/_unfinished/meetup/events_group_${parameters.group_id}.json`, JSON.stringify(resp, null, 2));
                console.log(`Saved events of group ID ${parameters.group_id}!`);
            }
        });
    },

    saveCityEvents: async function (city, country, state) {
        //save the latest top 200 groups, sorted by number of members
        await module.exports.saveGroups(city, country, state)

        await wait(10000)

        //load file with groups
        let groups = JSON.parse(fs.readFileSync(`../data/_unfinished/meetup/meetup_groups_${country}_${city}.json`));

        //create array for events
        let events = {
            result: []
        }

        //for every group => saveGroupEvents
        for (var i = 0; i < groups.result.length; i++) {
            let parameters = {
                group_id: groups.result[i].meetupID
            }

            //get all the events for the group
            console.log("wait 19 seconds for rate limit...")
            await wait(19000)
            meetup.getEvents(parameters, async function (err, resp) {
                if (err) {
                    console.log("There was an error...")
                } else {
                    // for every event in response, save it to events variable
                    for (event of resp.results) {
                        var nowUNIX = Math.round(new Date().getTime() / 1000);
                        var eventUNIX = event.time ? event.time > 1000000000000 ? event.time / 1000 : event.time : null;

                        //check if eventUnix is not null
                        if (eventUNIX) {
                            //only add event if its in the future & less then 30 days (2592000 seconds) ahead 
                            if ((eventUNIX > nowUNIX) && (eventUNIX < (nowUNIX + 2592001))) {
                                event = {
                                    name: event.name,
                                    slug: event.name,
                                    meetupID: event.id,
                                    typeName: "meetup",
                                    image: event.photo_url ? event.photo_url : groups.result[i - 1].group_photo ? (groups.result[i - 1].group_photo).photo_link : null || null,
                                    description: event.description || null,
                                    descriptionSourceName: "Meetup",
                                    eventUNIXtime: event.time ? event.time > 1000000000000 ? event.time / 1000 : event.time : null,
                                    UTCoffset: event.utc_offset,
                                    city: event.city,
                                    country: event.country,
                                    state: event.state ? event.state : null,
                                    by: event.group.name,
                                    meetupGroup: event.group.name ? {
                                        name: event.group.name,
                                        urlname: event.group.urlname,
                                        meetupID: event.group.id
                                    } : null,
                                    category: "Learning",
                                    IFcontentSource1: "Meetup",
                                    contenturl: event.event_url,
                                    releaseDate: event.created,
                                    PriceUSD: event.fee ? (event.fee).amount ? (event.fee).amount : 0 : 0,
                                    timeMinutes: (event.duration / 60000),
                                    time: (event.duration / 60000) > 1440 ? Math.round(((event.duration / 60000) / 1440)) : (event.duration / 60000) > 60 ? Math.round(((event.duration / 60000) / 60)) : (event.duration / 60000),
                                    timeUnitName: (event.duration / 60000) > 1440 ? "days" : (event.duration / 60000) > 60 ? "hours" : "minutes",
                                    eventVenue: event.venue ? {
                                        name: event.venue.name,
                                        meetupID: event.venue.id,
                                        address: event.venue.address_1,
                                        city: event.venue.city,
                                        state: event.venue.state ? event.venue.state : null,
                                        country: event.venue.country,
                                        lon: event.venue.lon,
                                        lat: event.venue.lat,
                                    } : null,
                                    eventRSVPlimit: event.rsvp_limit ? event.rsvp_limit : null,
                                    eventPlacesLeft: event.rsvp_limit ? (event.rsvp_limit - event.yes_rsvp_count - event.maybe_rsvp_count) : null,
                                    eventRSVPsYES: event.yes_rsvp_count ? event.yes_rsvp_count : null,
                                    eventRSVPsMAYBE: event.maybe_rsvp_count ? event.maybe_rsvp_count : null,
                                    eventRSVPs: event.yes_rsvp_count ? event.maybe_rsvp_count ? (event.yes_rsvp_count + event.maybe_rsvp_count) : event.yes_rsvp_count : event.maybe_rsvp_count ? event.maybe_rsvp_count : null,
                                    eventWaitlist: event.waitlist_count ? event.waitlist_count : null,
                                    meetuptopics: groups.result[i - 1].meetuptopics
                                }

                                event = await correctResult.correct(event)
                                event = await correctResult.deleteNullFields(event)
                                events.result.push(event)
                            }
                        }
                    }
                }
            });
            fs.writeFileSync(`../data/_unfinished/meetup/meetup_events_${country}_${city}.json`, JSON.stringify(events, null, 2));
            console.log(`Saved events_${country}_${city}.json ...`);
        }
    },

    saveEvents: async function () {
        //https://github.com/jkutianski/meetup-api/wiki/API-Methods-List#openevents


        ///////////////
        ///2018, June 3, 18:12 - doesn't work: doesn't give any output
        ///////////////        

        let parameters = {
            topic: "softwaredev",
            country: "US",
            limited_events: true
        }

        console.log("wait 19 seconds for rate limit...")
        await wait(19000)
        meetup.getStreamOpenEvents(parameters, function (err, resp) {
            if (err) {
                console.log("There was an error...")
            } else {
                fs.writeFileSync(`../data/_unfinished/meetup/events.json`, JSON.stringify(resp, null, 2));
                console.log(`Saved the events from 1 month ago up to 3 months ahead, with 3 RSVPS or more!`);
            }
        });
    },


    saveTopics: async function () {
        //https://github.com/jkutianski/meetup-api/wiki/API-Methods-List#topics

        let topics = {
            result: []
        }

        let search = ["robotics", "code", "software"]

        for (word of search) {

            let parameters = {
                search: word
            }

            console.log("wait 19 seconds for rate limit...")
            await wait(19000)
            meetup.getTopics(parameters, function (err, resp) {
                if (err) {
                    console.log("There was an error...")
                } else {
                    for (topic of resp.results) {
                        if (topic.members > 499) {
                            topics.result.push(topic)
                        }
                    }
                    fs.writeFileSync(`../data/_unfinished/meetup/topics.json`, JSON.stringify(topics, null, 2));
                    console.log("Saved meetup topics!");
                }
            });
        }
    }
}