// created by
// Marco Bartsch

const ShortenNoHTMLnoWrongSpaces = require("./ShortenNoHTMLNoWrongSpaces.js");
const fs = require("fs");
const config = require("../config.json")
const slack = require("../slack.js");
const synonyms = config.database_synonyms



module.exports = {
  correct: async function (result) {
    try {
      //make fields with empty objects or arrays = null
      for (emptyfields in result) {
        if ((typeof result[emptyfields] == "object" || typeof result[emptyfields] == "array") && result[emptyfields] != null) {
          if (result[emptyfields].toString() == "{}" || result[emptyfields].toString() == "[]") {
            result[emptyfields] == null
          }
        }
      }

      //set sortValue
      var sortValueStart = 0;

      if (result.typeName != "topic") {
        sortValueStart = sortValueStart + 100;
      }
      //highlight specific categories
      if (
        result.typeName == "operating system" ||
        result.typeName == "programming language" ||
        result.typeName == "framework" ||
        result.typeName == "book"
      ) {
        sortValueStart = sortValueStart + 5000;
      }

      //highlight specific sources
      if (
        result.IFcontentSource1 == "Udacity" ||
        result.IFcontentSource1 == "freeCodeCamp" ||
        result.IFcontentSource1 == "Codecademy"
      ) {
        sortValueStart = sortValueStart + 10000;
      }

      if (result.Kindle == true) {
        sortValueStart = sortValueStart + 200;
      }
      if (result.Print == true) {
        sortValueStart = sortValueStart + 200;
      }
      if (result.BestSeller == true) {
        sortValueStart = sortValueStart + 2000;
      }
      if (result.BestSellerRankInBooks < 10) {
        sortValueStart = sortValueStart + 2000;
      } else if (result.BestSellerRankInBooks < 50) {
        sortValueStart = sortValueStart + 1000;
      } else if (result.BestSellerRankInBooks < 100) {
        sortValueStart = sortValueStart + 500;
      } else if (result.BestSellerRankInBooks < 500) {
        sortValueStart = sortValueStart + 200;
      } else if (result.BestSellerRankInBooks < 1000) {
        sortValueStart = sortValueStart + 50;
      }

      //initiaze standard image array
      let typeName2image = {
        nanodegree: "images/nanodegree.svg",
        "online course": "images/onlinecourse.svg",
        "video tutorial": "images/videotutorial.svg",
        meetup: "images/meetupevent.svg",
        group: "images/meetupgroup.svg",
        book: "images/book.svg",
        topic: "images/topic.svg",
        technology: "images/api.svg",
        framework: "images/framework.svg",
        API: "images/api.svg",
        "programming language": "images/programminglanguage.svg",
        "operating system": "images/operatingsystem.svg",
        organization: "images/organization.svg",
        "project type": "images/projecttype.svg",
        database: "images/database.svg"
      };

      let source2image = {
        SkillMe: "images/HeySkillMe.svg",
        Wikipedia: "images/wikipedia.svg",
        "Stackshare.io": "images/stackshareio.png",
        Amazon: "images/amazon.svg",
        Udemy: "images/udemy.svg",
        Udacity: "images/udacity.svg",
        Codecademy: "images/codecademy.svg",
        freeCodeCamp: "images/freecodecamp.svg",
        "App Store": "images/apple.svg",
        "Google Play": "images/googleplay.svg",
        Meetup: "images/meetup.svg",
        YouTube: "images/youtube.svg",
        Pluralsight: "images/pluralsight.svg",
        teamtreehouse: "images/teamtreehouse.svg"
      }

      //load manual curated favorite list
      let favoriteTopics = require(`../data/favoriteTopics.json`)
      let favoriteContent = require(`../data/favoriteContent.json`)

      //check if results is on favorite list - if true make Recommended true
      if (favoriteTopics.some(topic => {
          return topic == result.slug
        })) {
        var Recommended = true
      } else if (favoriteContent.some(content => {
          return content == result.slug
        })) {
        var Recommended = true
      } else {
        var Recommended = false
      }

      //define level of content
      let beginnerKeywords = ["begin", "start", "newbie", "intro", "foundation", "scratch", "fundamentals", "basics", "dummie", "dummy", "from 0 to", "from zero to"];
      let intermediateKeywords = ["intermediate", "advance"];

      if (result.category) {
        var levelName = "all"
      } else {
        var levelName = null
      }

      for (intermediatekeyword of intermediateKeywords) {
        // if description & headline exists
        if (result.description && result.headline) {
          if (
            result.name.toLowerCase().includes(intermediatekeyword) ||
            result.description.toLowerCase().includes(intermediatekeyword) ||
            result.headline.toLowerCase().includes(intermediatekeyword)
          ) {
            levelName = "intermediate"
            break
          }
        } // if description exists
        else if (result.description) {
          if (
            result.name.toLowerCase().includes(intermediatekeyword) ||
            result.description.toLowerCase().includes(intermediatekeyword)
          ) {
            levelName = "intermediate"
            break
          }
        } // if headline exists
        else if (result.headline) {
          if (
            result.name.toLowerCase().includes(intermediatekeyword) ||
            result.headline.toLowerCase().includes(intermediatekeyword)
          ) {
            levelName = "intermediate"
            break
          }
        } //otherwise name only
        else {
          if (
            result.name.toLowerCase().includes(intermediatekeyword)
          ) {
            levelName = "intermediate"
            break
          }
        }
      }

      for (beginnerkeyword of beginnerKeywords) {
        // if description & headline exists
        if (result.description && result.headline) {
          if (
            result.name.toLowerCase().includes(beginnerkeyword) ||
            result.description.toLowerCase().includes(beginnerkeyword) ||
            result.headline.toLowerCase().includes(beginnerkeyword)
          ) {
            levelName = "beginner"
            break
          }
        } // if description exists
        else if (result.description) {
          if (
            result.name.toLowerCase().includes(beginnerkeyword) ||
            result.description.toLowerCase().includes(beginnerkeyword)
          ) {
            levelName = "beginner"
            break
          }
        } // if headline exists
        else if (result.headline) {
          if (
            result.name.toLowerCase().includes(beginnerkeyword) ||
            result.headline.toLowerCase().includes(beginnerkeyword)
          ) {
            levelName = "beginner"
            break
          }
        } //otherwise name only
        else {
          if (
            result.name.toLowerCase().includes(beginnerkeyword)
          ) {
            levelName = "beginner"
            break
          }
        }
      }



      if (result.releaseDate < 1000000000000) {
        var unix = new Date(result.releaseDate * 1000);
      } else {
        var unix = new Date(result.releaseDate);
      }

      if (typeof (result.releaseDate) == "string") {
        result.releaseDate = (new Date(result.releaseDate).getTime() / 1000)
      }

      let time = new Date

      //create array of people who created the content
      if (result.by && result.by.length > 0) {
        var by = result.by;
      } else {
        var by = [];
        if (result.byName) {
          by.push(result.byName);
          if (result.byName2) {
            by.push(result.byName2);
            if (result.byName3) {
              by.push(result.byName3);
              if (result.byName4) {
                by.push(result.byName4);
                if (result.byName5) {
                  by.push(result.byName5);
                }
              }
            }
          }
        }
      }

      //create array of usage scenarios of technologies on stackshare.io
      if (result.UsageScenarios && result.UsageScenarios.length > 0) {
        var UsageScenarios = result.UsageScenarios;
      } else {
        var UsageScenarios = [];
        if (result.UsageScenario1By) {
          UsageScenarios.push({
            By: result.UsageScenario1By || null,
            Text: result.UsageScenario1Text || null,
            Image: result.UsageScenario1ByPhoto || null
          });
          if (result.UsageScenario2By) {
            UsageScenarios.push({
              By: result.UsageScenario2By || null,
              Text: result.UsageScenario2Text || null,
              Image: result.UsageScenario2ByPhoto || null
            });
            if (result.UsageScenario3By) {
              UsageScenarios.push({
                By: result.UsageScenario3By || null,
                Text: result.UsageScenario3Text || null,
                Image: result.UsageScenario3ByPhoto || null
              });
              if (result.UsageScenario4By) {
                UsageScenarios.push({
                  By: result.UsageScenario4By || null,
                  Text: result.UsageScenario4Text || null,
                  Image: result.UsageScenario4ByPhoto || null
                });
                if (result.UsageScenario5By) {
                  UsageScenarios.push({
                    By: result.UsageScenario5By || null,
                    Text: result.UsageScenario5Text || null,
                    Image: result.UsageScenario5ByPhoto || null
                  });
                }
              }
            }
          }
        }
      }

      //define price
      let price = null
      if (!result.PriceUSD) {
        if (
          result.category == "Learning" ||
          result.typeName == "programming language" ||
          result.typeName == "framework" ||
          result.typeName == "database") {
          price = 0
        }
      }

      //match country based on countrycode
      let countryCode2Country = {
        de: "Germany",
        us: "USA",
        gb: "United Kingdom",
        fr: "France",
        in: "India",
        il: "Israel",
        hk: "Hong Kong",
        es: "Spain",
        cn: "China",
        at: "Austria"
      }

      result = {
        name: result.name,
        synonyms: synonyms[(result.name).toLowerCase()] ? synonyms[(result.name).toLowerCase()] : null,
        //if Udacity, then take slug from Udacity, if YouTube, take name + ID, if Amazon, take slug 
        slug: result.slug,
        LastUpdated: time,
        sortValue: 0,
        Recommended: Recommended,
        typeName: result.typeName || null,
        typeNameonStackshare: result.typeNameonStackshare || null,
        image: result.icon || result.image || typeName2image[result.typeName],
        descriptionSourceName: result.descriptionSourceName ? result.descriptionSourceName.length > 0 ? result.descriptionSourceName : null : null,
        descriptionSourcePictureURL: source2image[result.descriptionSourceName],
        description: result.description ? result.description.length > 0 ? result.description : null : null,
        headline: result.headline ? result.headline.length > 0 ? result.headline : null : null,
        Transcript: result.Transcript ? result.Transcript : null,
        curriculum: result.curriculum ? result.curriculum.length > 0 ? result.curriculum : null : null,
        country: result.eventVenue ? countryCode2Country[result.eventVenue.country.toLowerCase()] : result.country ? countryCode2Country[result.country.toLowerCase()] : null,
        city: result.eventVenue ? result.eventVenue.city : result.city ? result.city : null,
        state: result.eventVenue ? result.eventVenue.state ? result.eventVenue.state : result.state ? result.state : null : null,
        lon: result.eventVenue ? result.eventVenue.lon : result.lon ? result.lon : null,
        lat: result.eventVenue ? result.eventVenue.lat : result.lat ? result.lat : null,
        meetupID: result.meetupID ? result.meetupID : null,
        meetupurlname: result.meetupurlname ? result.meetupurlname : null,
        eventUNIXtime: result.eventUNIXtime ? result.eventUNIXtime : null,
        UTCoffset: result.UTCoffset ? result.UTCoffset.length > 0 ? result.UTCoffset : result.UTCoffset : null,
        eventVenue: result.eventVenue ? {
          name: result.eventVenue.name,
          meetupID: result.eventVenue.meetupID,
          address: result.eventVenue.address,
          city: result.eventVenue.city,
          state: result.eventVenue.state ? result.eventVenue.state : null,
          country: countryCode2Country[result.eventVenue.country.toLowerCase()],
          lon: result.eventVenue.lon,
          lat: result.eventVenue.lat,
        } : null,
        meetupGroup: result.meetupGroup ? result.meetupGroup : null,
        eventRSVPlimit: result.eventRSVPlimit ? result.eventRSVPlimit : null,
        eventPlacesLeft: result.eventPlacesLeft ? result.eventPlacesLeft > 0 ? result.eventPlacesLeft : 0 : null,
        eventRSVPsYES: result.eventRSVPsYES ? result.eventRSVPsYES : null,
        eventRSVPsMAYBE: result.eventRSVPsMAYBE ? result.eventRSVPsMAYBE : null,
        eventRSVPs: result.eventRSVPs ? result.eventRSVPs : null,
        eventWaitlist: result.eventWaitlist ? result.eventWaitlist : null,
        members: result.members ? result.members : null,
        releaseDate: result.releaseDate ? result.releaseDate.toString().length > 0 ? parseInt(result.releaseDate) : null : null,
        releaseYYYY: result.releaseDate ? result.releaseDate.toString().length > 0 ? unix.getFullYear() : null : null,
        by: by,
        category: result.category ? result.category.length > 0 ? result.category : null : null,
        contentcategory: result.category ? result.category == "Learning" ? result.typeName : null : null,
        topiccategory: result.category ? result.category == "Learning" ? null : result.typeName : result.typeName,
        IFcontentSource1: result.IFcontentSource1 ? result.IFcontentSource1.length > 0 ? result.IFcontentSource1 : null : null,
        contenturl: result.contenturl ? result.contenturl.length > 0 ? result.contenturl : null : null,
        IFcontentSource1PictureURL: source2image[result.IFcontentSource1],
        WikipediaURL: result.WikipediaURL ? result.WikipediaURL.length > 0 ? result.WikipediaURL : null : null,
        stackshareURL: result.stackshareURL ? result.stackshareURL.length > 0 ? result.stackshareURL : null : null,
        website: result.website ? result.website.length > 0 ? result.website : null : null,
        GitHub: result.GitHub ? result.GitHub.length > 0 ? result.GitHub : null : null,
        FavoriteNumber: result.FavoriteNumber ? result.FavoriteNumber.toString().length > 0 ? parseInt(result.FavoriteNumber) : null : null,
        votesNumber: result.votesNumber ? result.votesNumber.toString().length > 0 ? parseInt(result.votesNumber) : null : null,
        integrationsNumber: result.integrationsNumber ? result.integrationsNumber.toString().length > 0 ? parseInt(result.integrationsNumber) : null : null,
        GitHubStars: result.GitHubStars ? result.GitHubStars.toString().length > 0 ? parseInt(result.GitHubStars) : null : null,
        GitHubForks: result.GitHubForks ? result.GitHubForks.toString().length > 0 ? parseInt(result.GitHubForks) : null : null,
        fansNumber: result.fansNumber ? result.fansNumber.toString().length > 0 ? parseInt(result.fansNumber) : null : null,
        stacksNumber: result.stacksNumber ? result.stacksNumber.toString().length > 0 ? parseInt(result.stacksNumber) : null : null,
        upvotes: result.upvotes ? result.upvotes.toString().length > 0 ? parseInt(result.upvotes) : null : null,
        views: result.views ? result.views.toString().length > 0 ? parseInt(parseInt(result.views) / 20) : null : null,
        StudentsEnrolled: result.StudentsEnrolled ? result.StudentsEnrolled.toString().length > 0 ? parseInt(result.StudentsEnrolled) : null : null,
        bookmarkedNumber: result.bookmarkedNumber ? result.bookmarkedNumber.toString().length > 0 ? parseInt(result.bookmarkedNumber) : null : null,
        projectsNumber: result.projectsNumber ? result.projectsNumber.toString().length > 0 ? parseInt(result.projectsNumber) : null : null,
        averageRating: result.averageRating ? result.averageRating.toString().length > 0 ? parseFloat(result.averageRating).toFixed(1) : null : null,
        numberOfRatings: result.numberOfRatings ? result.numberOfRatings.toString().length > 0 ? parseInt(result.numberOfRatings) : null : null,
        numberOfLectures: result.numberOfLectures ? result.numberOfLectures.toString().length > 0 ? parseInt(result.numberOfLectures) : null : null,
        numberOfQuizzes: result.numberOfQuizzes ? result.numberOfQuizzes.toString().length > 0 ? parseInt(result.numberOfQuizzes) : null : null,
        HoursOnDemandVideo: result.HoursOnDemandVideo ? result.HoursOnDemandVideo.toString().length > 0 ? parseInt(result.HoursOnDemandVideo) : null : null,
        primary_category: result.primary_category ? result.primary_category.length > 0 ? result.primary_category : null : null,
        primary_subcategory: result.primary_subcategory ? result.primary_subcategory.length > 0 ? result.primary_subcategory : null : null,
        votesNumber: result.votesNumber ? result.votesNumber.toString().length > 0 ? parseInt(result.votesNumber) : null : null,
        fansNumber: result.fansNumber ? result.fansNumber.toString().length > 0 ? parseInt(result.fansNumber) : null : null,
        stacksNumber: result.stacksNumber ? result.stacksNumber.toString().length > 0 ? parseInt(result.stacksNumber) : null : null,
        WhyPeopleLikeItReason1: result.WhyPeopleLikeItReason1 || null,
        WhyPeopleLikeItReason2: result.WhyPeopleLikeItReason2 || null,
        WhyPeopleLikeItReason3: result.WhyPeopleLikeItReason3 || null,
        WhyPeopleLikeItReason4: result.WhyPeopleLikeItReason4 || null,
        WhyPeopleLikeItReason5: result.WhyPeopleLikeItReason5 || null,
        WhyPeopleLikeItReason6: result.WhyPeopleLikeItReason6 || null,
        WhyPeopleLikeItReason7: result.WhyPeopleLikeItReason7 || null,
        WhyPeopleLikeItReason8: result.WhyPeopleLikeItReason8 || null,
        WhyPeopleLikeItReason9: result.WhyPeopleLikeItReason9 || null,
        WhyPeopleLikeItReason10: result.WhyPeopleLikeItReason10 || null,
        UsageScenarios: UsageScenarios,
        new: result.new || false,
        Why: result.Why || null,
        Syllabus: result.Syllabus || null,
        collaborationPartner: result.collaborationPartner ? result.collaborationPartner.length > 0 ? result.collaborationPartner : null : null,
        levelName: levelName,
        time: result.time ? typeof result.time == "number" || typeof result.time == "string" && result.time.toString().length > 0 ? parseInt(result.time) : null : null,
        timeUnitName: result.timeUnitName || null,
        timeMinutes: result.timeMinutes ? result.timeMinutes.toString().length > 0 ? parseInt(result.timeMinutes) :
          (result.timeUnit && result.time) ?
          result.timeUnitName == "minutes" ? parseInt(result.time) :
          result.timeUnitName == "hours" ? parseInt(result.time) * 60 :
          result.timeUnitName == "days" ? parseInt(result.time) * 1440 :
          result.timeUnitName == "weeks" ? parseInt(result.time) * 10080 :
          result.timeUnitName == "months" ? parseInt(result.time) * 43800 :
          parseInt(result.time) : parseInt(result.time) : null,
        YouTubeID: result.YouTubeID ? result.YouTubeID.toString().length > 0 ? result.YouTubeID : null : null,
        TrailerIDVimeo: result.TrailerIDVimeo ? result.TrailerIDVimeo.toString().length > 0 ? result.TrailerIDVimeo : null : null,
        BestSeller: (result.BestSeller && result.BestSeller == true) || false,
        BestSellerIn: result.BestSellerIn || null,
        ISBN10: result.ISBN10 || null,
        timeline: result.timeline ? result.timeline.length > 0 ? result.timeline : null : null,
        booktype: result.Kindle == true && result.Print == true ? ["eBook", "print"] : result.Kindle == true ? "eBook" : result.Print == true ? "print" : [],
        PriceUSD: result.PriceUSD ? result.PriceUSD.toString().length > 0 && parseInt(result.PriceUSD) != 1 ? parseFloat(result.PriceUSD) : price : price,
        PriceRange: null,
        Options: result.Options ? result.Options : null,
        appstoreID: result.appstoreID || null,
        icon: result.icon || null,
        primaryGenre: result.primaryGenre || null,
        contentRating: result.contentRating || null,
        languages: result.languages || [],
        sizeinMB: result.sizeinMB || null,
        installs: result.installs || null,
        requiredOsVersion: result.requiredOsVersion || null,
        genres: result.genres || [],
        developerId: result.developerId || null,
        developer: result.developer || null,
        developerUrl: result.developerUrl || null,
        developerWebsite: result.developerWebsite || null,
        screenshots: result.screenshots || [],
        ipadScreenshots: result.ipadScreenshots || [],
        supportedDevices: result.supportedDevices || [],
        meetuptopics: result.meetuptopics || null
      }

      //add eventUNIXtimeEND
      result.eventUNIXtimeEND = result.eventUNIXtime ? (result.eventUNIXtime + (result.timeMinutes * 60)) : 4102358400 //placeholder for Dec 31 2099, to make sure result always shows up

      //if amazon: select lowest price from different options (Kindle, Paperback, etc.)
      if (result.IFcontentSource1 == "Amazon" && result.Options) {
        var lowest = Number.POSITIVE_INFINITY;
        var tmp;
        for (var i = result.Options.length - 1; i >= 0; i--) {
          tmp = result.Options[i].price;
          if (tmp < lowest) lowest = tmp;
        }
        result.PriceUSD = lowest;
      }

      //define PriceRange
      if (parseInt(result.PriceUSD) == 0) {
        result.PriceRange = "free"
      } else if (parseInt(result.PriceUSD) > 0 && parseInt(result.PriceUSD) < 26) {
        result.PriceRange = "$1 - $25"
      } else if (parseInt(result.PriceUSD) > 25 && parseInt(result.PriceUSD) < 101) {
        result.PriceRange = "$25 - $100"
      } else if (parseInt(result.PriceUSD) > 100 && parseInt(result.PriceUSD) < 501) {
        result.PriceRange = "$100 - $500"
      } else if (parseInt(result.PriceUSD) > 500) {
        result.PriceRange = "> $500"
      }

      //if Udacity content:
      if (result.IFcontentSource1 == "Udacity") {
        result.slug = `${result.typeName}-${result.contenturl.match(/\/([^\/]*)$/)[1]}`
          .toLowerCase()
          .replace(/#/g, "sharp")
          .replace(/\+/g, "plus")
          .replace(/[^a-zA-Z\d\s]/g, "-")
          .replace(/\s/g, "-")
          .replace(/\-{1,}/g, "-")
          .replace(/\-$/g, "")
      } //if meetup content:
      else if (result.typeName == "meetup") {
        result.slug = `${result.typeName}-${result.name}-${result.eventUNIXtime}`
          .toLowerCase()
          .replace(/#/g, "sharp")
          .replace(/\+/g, "plus")
          .replace(/[^a-zA-Z\d\s]/g, "-")
          .replace(/\s/g, "-")
          .replace(/\-{1,}/g, "-")
          .replace(/\-$/g, "")
      } //if other content:
      else if (result.IFcontentSource1) {
        result.slug = `${result.typeName}-${result.name}`
          .toLowerCase()
          .replace(/#/g, "sharp")
          .replace(/\+/g, "plus")
          .replace(/[^a-zA-Z\d\s]/g, "-")
          .replace(/\s/g, "-")
          .replace(/\-{1,}/g, "-")
          .replace(/\-$/g, "")
      } //if no content:
      else {
        result.slug = result.name
          .toLowerCase()
          .replace(/#/g, "sharp")
          .replace(/\+/g, "plus")
          .replace(/[^a-zA-Z\d\s]/g, "-")
          .replace(/\s/g, "-")
          .replace(/\-{1,}/g, "-")
          .replace(/\-$/g, "")
      }

      //no HTML, shorten and no wrong spaces
      result.descriptionShortened = ShortenNoHTMLnoWrongSpaces.return(result.description).shortened;
      //if description doesn't already include a ... at the end, then shorten it, otherwise keep it
      if (result.description) {
        if (result.description.endsWith("...")) {
          //keep existing description
        } else if (result.descriptionShortened == true) {
          result.description = `${ShortenNoHTMLnoWrongSpaces.return(result.description).newText} ...`;
        } else {
          result.description = ShortenNoHTMLnoWrongSpaces.return(result.description).newText;
        }
      }
      result.Transcript = result.Transcript != null ? ShortenNoHTMLnoWrongSpaces.noHTMLnoWrongSpaces(result.Transcript) : null;

      //highlight beginner courses
      if (result.levelName == "beginner") {
        sortValueStart = sortValueStart + 5000
      }


      async function calcSortValue() {
        result.sortValue = (
          sortValueStart +
          result.FavoriteNumber +
          result.votesNumber +
          result.integrationsNumber +
          result.GitHubStars +
          result.GitHubForks +
          result.fansNumber +
          result.stacksNumber +
          result.upvotes +
          result.views +
          result.StudentsEnrolled +
          result.bookmarkedNumber +
          result.projectsNumber +
          parseInt(result.averageRating * result.numberOfRatings) +
          result.numberOfLectures +
          result.numberOfQuizzes +
          result.HoursOnDemandVideo +
          result.members +
          parseInt(result.eventRSVPs * 100)
        );
      }
      await calcSortValue();

      //return updated result
      return result;
    } catch (error) {
      console.log(error);
      slack.post(error)
      // errorhandler.push("correctResult.js", "correctResult", error, result)
    }
  },
  deleteNullFields: function (result) {
    //delete null fields in result before pushing to Algolia => better overview & faster loading
    for (propName in result) {
      if (result[propName] === null || result[propName] === undefined || typeof result[propName] == "object" && result[propName].length == 0) {
        delete result[propName];
      }
    }
    return result
  }
};