// created by
// Marco Bartsch

//https://github.com/facundoolano/app-store-scraper/blob/master/README.md

const store = require("app-store-scraper");
const fs = require("fs");
const ShortenNoHTMLnoWrongSpaces = require("./ShortenNoHTMLNoWrongSpaces.js");
const franc = require("franc");
const slack = require("../slack.js");
const cron = require("node-cron");

function wait(ms) {
  return new Promise(resolve => {
    setTimeout(() => resolve(), ms);
  });
}


alwaysUpdate()

//send once a day notification that script is still running
cron.schedule("0 9 * * *", function () {
  console.log("✅ All fine, appstore.js is running.")
  slack.post("✅ All fine, appstore.js is running.")
})

async function alwaysUpdate() {
  for (;;) {
    await search()
    slack.post("✅ Updated AppStore")
  }
}

async function search() {
  try {
    let dontinclude = [
      "Games",
      "Entertainment",
      "Productivity",
      "Child",
      "Kindergarten",
      "Pimp",
      "Keyboard",
      "File Manager",
      "Downloader",
      "Browser",
      "Pocket Guide",
      "Pokemon",
      "connect Wi-Fi",
      "Transfer files",
      "sync files",
      "Wallpapers",
      "iCreate",
      "pet",
      "Reptile",
      "Morphlist",
      "Christian",
      "Religion",
      "Horror",
      "College",
      "Stir Trek",
      "Minecraft",
      "cloudLibrary",
      "school",
      "Homework"
    ]

    //search these topics and save the apps
    let array = [
      "Python",
      "learn to code",
      "coding",
      "Web development",
      "Ruby",
      "app development",
      "Swift",
      "HTML",
      "CSS",
      "JavaScript",
      "SQL",
      "MongoDB",
      "Bootstrap",
      "PHP",
      "Node.js",
      "game development",
      "programming language",
      "framework",
      "cloud",
      "AI",
      "Ubuntu",
      "Linux",
      "Mac",
      "iOS",
      "Android",
      "C++"
    ];

    let numberResults = 200;

    let results = []

    for (term of array) {
      await store
        .search({
          term: term,
          num: numberResults,
          device: store.device.ALL,
          country: "us"
        })
        .then(function (data) {
          //filter things that have to be included
          let output = data.filter(function (val) {
            return (
              //what must be included...
              ((val.languages.includes("EN") == true && val.primaryGenre == "Education") ||
                val.primaryGenre == "Book") &&

              //...average rating need to be higher then 3
              (val.currentVersionReviews || val.reviews) &&
              (!(val.currentVersionScore < 3) || !(val.score < 3))
            )
          })

          //make sure these paramters are not included
          for (words in dontinclude) {
            output = output.filter(function (val) {
              return val.genres.includes(dontinclude[words]) == false &&
                val.genres.includes(dontinclude[words].toLowerCase()) == false &&
                val.title.includes(dontinclude[words]) == false &&
                val.title.includes(dontinclude[words].toLowerCase()) == false &&
                val.description.includes(dontinclude[words]) == false &&
                val.description.includes(dontinclude[words].toLowerCase()) == false &&
                val.appId.includes(dontinclude[words]) == false &&
                val.appId.includes(dontinclude[words].toLowerCase()) == false
            })
          }
          for (app of output) {
            //make sure to exclude apps in other languages then english
            if (franc(app.title) != "cmn" &&
              franc(app.title) != "jpn" &&
              franc(app.title) != "kor" &&
              franc(app.title) != "vie" &&
              franc(app.title) != "tha" &&
              franc(app.title) != "und" &&
              franc(app.title) != "zlm" &&
              franc(app.title) != "arb") {
              if (app.updated) {
                var date = new Date(app.updated.split(" ").join("T"));
                date = date.getTime() / 1000;
              } else {
                var date = new Date(app.released.split(" ").join("T"));
                date = date.getTime() / 1000;
              }

              //push app data to array
              results.push({
                name: app.title,
                IFcontentSource1: "App Store",
                contenturl: app.url,
                typeName: "app",
                slug: "app-" +
                  app.title
                  .toLowerCase()
                  .replace(/[^\w ]+/g, "")
                  .replace(/ +/g, "-"),
                category: "Learning",
                image: app.icon,
                PriceUSD: parseFloat(app.price),
                description: ShortenNoHTMLnoWrongSpaces.return(app.description).newText,
                descriptionSourceName: "App Store",
                DescriptionExtended: app.description,
                releaseDate: date,
                numberOfRatings: app.currentVersionReviews || app.reviews,
                averageRating: app.currentVersionScore || app.score,
                appstoreID: app.id,
                primaryGenre: app.primaryGenre,
                contentRating: app.contentRating,
                languages: app.languages,
                sizeinMB: parseFloat((app.size / 1000000).toFixed(2)),
                requiredOsVersion: parseFloat(app.requiredOsVersion),
                genres: app.genres,
                contentRating: app.contentRating,
                developerId: app.developerId,
                developer: app.developer,
                developerUrl: app.developerUrl,
                developerWebsite: app.developerWebsite,
                screenshots: app.screenshots,
                ipadScreenshots: app.ipadScreenshots,
                appletvScreenshots: app.appletvScreenshots,
                supportedDevices: app.supportedDevices
              });
            }
          }
          console.log("Updating AppStoreiOSMac.json...")
          if (results.length < 100) {
            fs.writeFileSync(`../data/_preview/AppStoreiOSMac.json`, JSON.stringify(results, null, 2));
          }
        })
        .catch();
    }
    fs.writeFileSync(`../data/_unfinished/AppStoreiOSMac.json`, JSON.stringify(results, null, 2));
    console.log("Updated AppStoreiOSMac.json! Wait now for 5 hours!")
    await wait(18000000)

  } catch (error) {
    console.log(error);
    slack.post(error)
  }
}