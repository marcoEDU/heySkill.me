// created by
// Marco Bartsch

const striptags = require("striptags");
const sanitizeHtml = require("sanitize-html");

module.exports = {
  return: function (text) {
    //if description text is short, save it without HTML tags, otherwise shorten it first, then save
    function getWords(str) {
      return str
        .split(/\s+/)
        .slice(0, 55)
        .join(" ");
    }

    function getWordsLong(str) {
      return str
        .split(/\s+/)
        .slice(0, 1500)
        .join(" ");
    }

    if (
      striptags(text)
      .replace(/\./g, ". ")
      .replace(/\?/g, "? ")
      .replace(/\s{2,}/g, " ")
      .replace(/&quot;/g, '"').length ==
      getWords(striptags(text))
      .replace(/\./g, ". ")
      .replace(/\?/g, "? ")
      .replace(/\s{2,}/g, " ")
      .replace(/&quot;/g, '"').length
    ) {
      var newText = getWords(striptags(text))
        .replace(/\./g, ". ")
        .replace(/\?/g, "? ")
        .replace(/\s{2,}/g, " ")
        .replace(/&quot;/g, '"');
      var shortened = false;
    } else {
      var newText = getWords(striptags(text))
        .replace(/\./g, ". ")
        .replace(/\?/g, "? ")
        .replace(/\s{2,}/g, " ")
        .replace(/&quot;/g, '"');
      var shortened = true;
    }
    newText = module.exports.noHTMLnoWrongSpaces(newText);

    return {
      newText: newText,
      shortened: shortened
    };
  },
  noHTMLnoWrongSpaces: function (text) {
    //delete wrong spaces at beginning and end
    var newText = sanitizeHtml(striptags(text)).replace(/^\s\s*/, "").replace(/\s\s*$/, "")
      .replace(/\./g, ". ")
      .replace(/\?/g, "? ")
      .replace(/\s{2,}/g, " ")
      .replace(/&quot;/g, '"')

    return newText;
  }
};