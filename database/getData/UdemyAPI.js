// created by
// Marco Bartsch

const config = require("../config");
const fs = require("fs");
const cron = require("node-cron");
const Udemy = require("udemy-api");
const ShortenNoHTMLnoWrongSpaces = require("./ShortenNoHTMLNoWrongSpaces.js");
const slack = require("../slack.js");

function wait(ms) {
  return new Promise(resolve => {
    setTimeout(() => resolve(), ms);
  });
}

udemyApiClient = new Udemy(config.UdemyAPI_ClientID, config.UdemyAPI_ClientSecret);



alwaysUpdate()

//send once a day notification that script is still running
cron.schedule("0 9 * * *", function () {
  console.log("✅ All fine, UdemyAPI.js is running.")
  slack.post("✅ All fine, UdemyAPI.js is running.")
})

async function alwaysUpdate() {
  for (;;) {
    await getData()
    console.log("✅ Updated Udemy")
    slack.post("✅ Updated Udemy")
  }
}


async function getData() {
  console.log("Start: UdemyAPI.getData")

  let results = [];
  const maxResult = 4500;
  let parsehub = require("../data/_unfinished/UdemyParsehub/Udemy.json");
  let parsehubMatch = null;


  await loop();

  fs.writeFileSync(`../data/_unfinished/Udemy.json`, JSON.stringify(results, null, 2));


  async function loop() {
    for (result = 1; result < maxResult; result++) {
      try {
        //get all the courses with detailed info
        udemyApiClient.get(
          `courses?page=${result}&page_size=1&category=Development&language=en&fields[course]=title,headline,description,avg_rating,created,image_480x270,price,url,num_lectures,num_quizzes,num_reviews,num_subscribers,status_label,visible_instructors,archive_time,completion_ratio,created,favorite_time,is_in_content_subscription,locale,primary_category,primary_subcategory,status_label&fields[course_review]=content,course,created,rating`,
          async function (err, res, body) {
            if (err) {
              return console.error(err);
            }
            try {

              let course = JSON.parse(body).results[0]

              let titleLowercase = course.title.toLowerCase();

              parsehubMatch = parsehub.result.find(function (resultToFind) {
                return resultToFind.name == course.title
              });

              //save single result
              results.push({
                name: course.title,
                contenturl: `https://www.udemy.com${course.url}`,
                image: course.image_480x270,
                levelName: titleLowercase.includes("beginner") ?
                  "beginner" : titleLowercase.includes("newbie") ?
                  "beginner" : titleLowercase.includes("introduction") ?
                  "beginner" : titleLowercase.includes("from scratch") ?
                  "beginner" : titleLowercase.includes("fundamentals") ?
                  "beginner" : titleLowercase.includes("basics") ?
                  "beginner" : titleLowercase.includes("getting started") ?
                  "beginner" : titleLowercase.includes("get started") ?
                  "beginner" : titleLowercase.includes("intermediate") ?
                  "intermediate" : titleLowercase.includes("advanced") ? "intermediate" : "all",
                Language: "English",
                udemy_id: course.id,
                slug: `online-course-${course.title
                    .toLowerCase()
                    .replace(/[^\w ]+/g, "")
                    .replace(/ +/g, "-")}`,
                IFcontentSource1: "Udemy",
                IFcontentSource1PictureURL: "images/udemy.svg",
                typeName: "online course",
                category: "Learning",
                releaseDate: new Date(course.created).getTime() / 1000,
                byName: course.visible_instructors[0].title,
                byImage: course.visible_instructors[0].image_100x100,
                headline: course.headline,
                time: parsehubMatch ? parsehubMatch.time ? parsehubMatch.time : null : null,
                timeUnit: parsehubMatch ? parsehubMatch.timeUnit ? parsehubMatch.timeUnit : null : null,
                timeUnitName: parsehubMatch ? parsehubMatch.timeUnitName ? parsehubMatch.timeUnitName : null : null,
                timeMinutes: parsehubMatch ? parsehubMatch.timeMinutes ? parsehubMatch.timeMinutes : null : null,
                level: parsehubMatch ? parsehubMatch.level ? parsehubMatch.level : null : null,
                levelName: parsehubMatch ? parsehubMatch.levelName ? parsehubMatch.levelName : null : null,
                description: ShortenNoHTMLnoWrongSpaces.return(course.description).newText,
                descriptionShortened: ShortenNoHTMLnoWrongSpaces.return(course.description).shortened,
                descriptionSourceName: "Udemy",
                descriptionExtended: ShortenNoHTMLnoWrongSpaces.noHTMLnoWrongSpaces(course.description),
                averageRating: parseFloat(course.avg_rating.toFixed(1)),
                numberOfRatings: course.num_reviews,
                StudentsEnrolled: course.num_subscribers,
                numberOfQuizzes: course.num_quizzes,
                numberOfLectures: course.num_lectures,
                PriceUSD: parsehubMatch ? parsehubMatch.PriceUSD ? parseFloat(parsehubMatch.PriceUSD) : null : null,
                primary_category: course.primary_category.title,
                primary_subcategory: course.primary_subcategory.title,
                curriculum: []
              });

            } catch (error) {
              console.log(error);
              slack.post(error)
              await wait(10000)
            }

          }
        );

        if (results.length < 100) {
          fs.writeFileSync(`../data/_preview/Udemy.json`, JSON.stringify(results, null, 2));
        }

        //Prevent Udemy API limit
        // 2 seconds => doesn't work
        // 5 seconds => doesn't work
        // 6 seconds => doesn't work
        // 10 seconds => works
        await wait(10000)
        console.log(`"Updating Udemy.json..."`)
      } catch (error) {
        console.log(error);
        slack.post(error)
      }
    }
  }


  console.log("Finished: UdemyAPI.getData")
}