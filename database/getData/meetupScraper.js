// created by
// Marco Bartsch

const cron = require("node-cron");
const meetup = require("./meetupAPI.js");
const slack = require("../slack.js");


alwaysUpdate()

async function alwaysUpdate() {
    for (;;) {
        await scrape()
        console.log("✅ Updated meetups")
        slack.post("✅ Updated meetups")
    }
}

//send once a day notification that script is still running
cron.schedule("0 9 * * *", function () {
    console.log("✅ All fine, meetupScraper.js is running.")
    slack.post("✅ All fine, meetupScraper.js is running.")
})

async function scrape() {
    //save all the latest groups & events
    await meetup.saveAllUpcomingEvents()
    await meetup.copyAllFiles("../data/_unfinished/meetup/", "../data/_unfinished/");
}