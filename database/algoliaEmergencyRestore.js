// created by
// Marco Bartsch

const cron = require("node-cron");
const config = require("./config");
const algolia = require("./algolia.js");
const algoliasearch = require("algoliasearch");
const client = algoliasearch(config.Algolia_ApplicationID, config.Algolia_APIkey);
const slack = require("./slack.js");


function wait(ms) {
    return new Promise(resolve => {
        setTimeout(() => resolve(), ms);
    });
}

//send once a day notification that script is still running
cron.schedule("0 9 * * *", function () {
    console.log("✅ All fine, algoliaEmergencyRestore script is running.")
    slack.post("✅ All fine, algoliaEmergencyRestore script is running.")
})

//every 10 minutes: check if SkillMe or SkillMeTopics database are empty, if true => auto restore
cron.schedule("*/10 * * * *", async function () {

    let indexes = ["SkillMe", "SkillMeTopics"];
    var browser;
    var index;
    var hits;

    for (indexName of indexes) {
        index = client.initIndex(indexName);

        browser = index.browseAll();
        hits = [];

        browser.on("result", function onResult(content) {
            hits = hits.concat(content.hits);
        });

        browser.on("end", function onEnd() {
            hits = {
                result: hits
            };

            //check if hits are empty
            try {
                if (hits.result.length < 10) {
                    emergencyRestore();
                } else {
                    console.log(`✅ ${indexName} database fine!`)
                }
            } catch (error) {
                emergencyRestore();
            }

            async function emergencyRestore() {
                //console.log & slack notification!
                console.log(`ERROR: ${indexName} database unexpectedly empty! Initiate backup restore...`)
                slack.post(`ERROR: ${indexName} database unexpectedly empty! Initiate backup restore...`)

                let backup = require(`./deploy/backup/algolia${indexName}Backup.json`)

                //if true => restore backup
                algolia.clearIndex(indexName)
                algolia.importLargeFile(backup.result, indexName)
                await wait(15000)

                index = client.initIndex(indexName);

                browser = index.browseAll();
                hits = [];

                browser.on("result", function onResult(content) {
                    hits = hits.concat(content.hits);
                });

                browser.on("end", function onEnd() {
                    hits = {
                        result: hits
                    }

                    //check if hits are still empty
                    if (hits.result.length < 10) {
                        //console.log & slack notification!
                        console.log(`ERROR: ${indexName} database automatic restore failed!!! Take action now!`)
                        slack.post(`ERROR: ${indexName} database automatic restore failed!!! Take action now!`)
                    } else {
                        //console.log & slack notification!
                        console.log(`RESTORED: ${indexName} database automatic automatically restored.`)
                        slack.post(`RESTORED: ${indexName} database automatic automatically restored.`)
                    }
                })
            }
        });

        browser.on("error", function onError(err) {
            throw err;
        });
        await wait(15000)
    }
});