// based on https://github.com/pCloud/pcloud-sdk-js/blob/master/examples/node/token.js
// changed by Marco Bartsch


var prompt = require('prompt');
var fs = require('fs');
var url = require('url');
var oauth = require('pcloud-sdk-js').oauth;
var config = require('../config.json');


if (!config.pCloud_ClientID || !config.pCloud_ClientID.length ||
    !config.pCloud_AppSecret || !config.pCloud_AppSecret.length
) {
    console.error("Required `client_id` and `app_secret` from `app.json`.");
    return;
}

const oauthUrl = url.format({
    protocol: 'https',
    hostname: 'my.pcloud.com',
    pathname: '/oauth2/authorize',
    query: {
        client_id: config.pCloud_ClientID,
        response_type: 'code'
    }
});

console.log('1. Open this url, you may have to login.');
console.log('============');
console.log(oauthUrl);
console.log('============');
console.log('2. Go trough the process, you will see a code.');
console.log('3. Enter the code');
console.log("\r\n");

//require("openurl").open(oauthUrl);

go();

function go() {
    prompt.start();
    prompt.get(['code'], function (error, result) {
        if (result && result.code) {
            oauth.getTokenFromCode(result.code, config.pCloud_ClientID, config.pCloud_AppSecret)
                .then((res) => {
                    console.log('Received token: ', res.access_token);

                    config.pCloud_access_token = res.access_token;
                    config.pCloud_userid = res.userid;
                    fs.writeFileSync('../config.json', JSON.stringify(config));
                    console.log("Info saved to `config.json`");
                }).catch((res) => {
                    console.error(res.error);
                    console.log("\r\n");
                    go();
                });

        } else if (result) {
            go();
        }
    });
}