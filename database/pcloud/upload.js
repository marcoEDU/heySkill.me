// based on https://github.com/pCloud/pcloud-sdk-js/blob/master/examples/node/upload.js
// changed by Marco Bartsch


const pcloud = require('pcloud-sdk-js')
const invariant = require('invariant');
var config = require('../config.json');
var fileoverview = require('./fileoverview.json');

invariant(config, 'Missing `config.json`.');
invariant(config.pCloud_access_token, 'Missing `pCloud_access_token` in `config.json`. Run `node token.js`');

const client = pcloud.createClient(config.pCloud_access_token);
const {
    upload,
    setupProxy
} = client;


uploadExample()
    .then(() => console.log('proxy upload'))
    .then(proxyUploadExample)
    .then(({
        metadata,
        checksums
    }) => {
        fileoverview.fileid = metadata.fileid;
        fileoverview.filename = metadata.name;

        saveAppJson(fileoverview);

        console.log(`Saved fileid: ${fileoverview.fileid} to fileoverview.json. You can now run 'node download.js'`);
    })
    .catch((err) => {
        console.log(err);
    });

//proxyUploadExample();
//uploadExample();

function uploadExample() {
    return upload('./deploy/data/content/Codecademy.json', 0, {
        onBegin: () => console.log('begin'),
        onProgress: ({
            loaded,
            total
        }) => console.log(loaded, total, (loaded / total * 100).toFixed(2) + '%')
    }).then((result) => {
        console.log("Done");
        return result;
    });
}

function withProxy(func) {
    return setupProxy()
        .then(func);
}

function proxyUploadExample() {
    return withProxy(uploadExample);
}

function saveAppJson(fileoverview) {
    require("fs").writeFileSync('./fileoverview.json', JSON.stringify(fileoverview));
}