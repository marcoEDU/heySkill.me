// created by
// Marco Bartsch

const config = require("./config");
var algoliasearch = require("algoliasearch");
var client = algoliasearch(config.Algolia_ApplicationID, config.Algolia_APIkey);
var index = client.initIndex(config.Algolia_Project);
var now = require("performance-now");
const fs = require("fs");
const chunk = require('lodash.chunk')
const slack = require("./slack.js");




function wait(ms) {
  return new Promise(resolve => {
    setTimeout(() => resolve(), ms);
  });
}
///////////////////////////////////////////////////
// create item in Algolia
///////////////////////////////////////////////////

module.exports = {
  addToSpecialHints: async function (result) {
      index = client.initIndex("specialHints");

      result = {
        name: result.name,
        description: result.summary,
        message: result.AlgoliaGuideMessage,
        CTA: result.AlgoliaCTAGetStarted == "true" ? "Get started" : null,
        messageCategory: "Blog",
        sortValue: 0,
        image: result.previewImage,
        link: `/blog/${result.slug}`,
        topics: result.AlgoliaTopics,
      }

      await index.addObject(result,
        function (err, content) {
          if (err) {
            //send message to slack/errorhandler - couldnt add to algolia
            console.log(err);
            slack.post(err)
          }
          console.log(`✅ Published blog post on SkillMe "${result.name}"`);
          slack.post(`✅ Published blog post on SkillMe "${result.name}"`);
        }
      );
    },
    importLargeFile: async function (file, indexName) {
        var index = client.initIndex(indexName);
        //create batch of 1000 results => import not more then 1000 results at the same time
        const chunks = chunk(file, 1000);
        chunks.map(function (batch) {
          return index.addObjects(batch);
        });
      },

      deleteResultbyName: async function (name) {
          let resultsContent = JSON.parse(fs.readFileSync("./data/_backup/algoliaSkillMeBackup.json"));
          let resultsTopics = JSON.parse(fs.readFileSync("./data/_backup/algoliaSkillMeTopicsBackup.json"));

          let algolia = {
            "result": resultsContent.result.concat(resultsTopics.result)
          };

          let algoliaresults = algolia.result

          function checkifexist(object) {
            return object.name == name;
          }

          function findID() {
            var found = algolia.result.find(checkifexist)
            module.exports.deleteResult([found.objectID], found.contentcategory ? "SkillMeBackup" : "SkillMeTopicsBackup")

          }

          findID();

        },

        checkIfExist: async function (result) {
            let resultsContent = JSON.parse(fs.readFileSync("./data/_backup/algoliaSkillMeBackup.json"));
            let resultsTopics = JSON.parse(fs.readFileSync("./data/_backup/algoliaSkillMeTopicsBackup.json"));

            let algolia = {
              "result": resultsContent.result.concat(resultsTopics.result)
            };

            var check = "not exist";

            //check if result exists
            if (algolia.result.some(algolia => {
                return algolia.name == result.name
              })) {
              check = "result exists"

            }
            return check
          },

          deleteNonExisting: async function () {
              try {
                let resultsContent = JSON.parse(fs.readFileSync("./data/_backup/algoliaSkillMeBackup.json"));
                let resultsTopics = JSON.parse(fs.readFileSync("./data/_backup/algoliaSkillMeTopicsBackup.json"));

                let algolia = {
                  "result": resultsContent.result.concat(resultsTopics.result)
                };

                let topics = JSON.parse(fs.readFileSync("./deploy/data/topics.json"));
                let Udacity = JSON.parse(fs.readFileSync("./deploy/data/content/Udacity.json"));
                let Udemy = JSON.parse(fs.readFileSync("./deploy/data/content/Udemy.json"));
                let Codecademy = JSON.parse(fs.readFileSync("./deploy/data/content/Codecademy.json"));
                let Amazon = JSON.parse(fs.readFileSync("./deploy/data/content/Amazon.json"));
                let freeCodeCamp = JSON.parse(fs.readFileSync("./deploy/data/content/freeCodeCamp.json"));

                let local = {
                  "result": topics.result.concat(Udacity.result, Udemy.result, Codecademy.result, Amazon.result, freeCodeCamp.result)
                };

                for (algolia of algolia.result) {
                  try {
                    if (await local.result.some(local => {
                        return local.id_algolia == algolia.objectID
                      }) == false) {
                      console.log(`Deleting from Algolia: ${algolia.name}`)
                      await module.exports.deleteResult([algolia.objectID], algolia.contentcategory ? "SkillMeBackup" : "SkillMeTopicsBackup")
                    }

                  } catch (error) {
                    console.log(error);
                    slack.post(error)
                  }
                };
              } catch (error) {
                console.log(error);
                slack.post(error)
              }

            },

            deleteByFieldContent: async function (fieldname, content) {
                try {
                  let resultsContent = JSON.parse(fs.readFileSync("./deploy/backup/algoliaSkillMeBackup.json"));
                  let resultsTopics = JSON.parse(fs.readFileSync("./deploy/backup/algoliaSkillMeTopicsBackup.json"));

                  let algolia = {
                    "result": resultsContent.result.concat(resultsTopics.result)
                  };

                  for (x in algolia.result) {
                    if (algolia.result[x][fieldname] == content) {
                      index.deleteObjects([algolia.result[x].objectID], function (err, content) {
                        console.log(content);
                      });
                    }
                  }
                } catch (error) {
                  console.log(error);
                  slack.post(error)
                }

              },

              deleteByField: async function (fieldname) {
                  try {
                    let resultsContent = JSON.parse(fs.readFileSync("./deploy/backup/algoliaSkillMeBackup.json"));
                    let resultsTopics = JSON.parse(fs.readFileSync("./deploy/backup/algoliaSkillMeTopicsBackup.json"));

                    let algolia = {
                      "result": resultsContent.result.concat(resultsTopics.result)
                    };
                    for (x in algolia.result) {
                      if (algolia.result[x][fieldname]) {
                        index.deleteObjects([algolia.result[x].objectID], function (err, content) {
                          console.log(content);
                        });
                      }
                    }
                  } catch (error) {
                    console.log(error);
                    slack.post(error)
                  }

                },

                exportSynonyms: async function (indexName) {
                    try {
                      index = client.initIndex(indexName);

                      let synonyms = await index.exportSynonyms(100);

                      if (synonyms) {
                        console.log(`Exported ${indexName} synonyms!`);
                        fs.writeFile(`./data/_backup/algolia${indexName}SynonymsBackup.json`, JSON.stringify(synonyms, null, 2));
                      } else {
                        console.log(`No synonyms saved in ${indexName}!`);
                      }
                    } catch (error) {
                      console.log(error);
                      slack.post(error)
                    }
                  },

                  exportQueryRules: async function (indexName) {
                      try {
                        index = client.initIndex(indexName);

                        let queryrules = await index.exportRules(100);

                        if (queryrules) {
                          console.log(`Exported ${indexName} queryrules!`);
                          fs.writeFile(`./data/_backup/algolia${indexName}QueryRulesBackup.json`, JSON.stringify(queryrules, null, 2));
                        } else {
                          console.log(`No queryrules saved in ${indexName}!`);
                        }
                      } catch (error) {
                        console.log(error);
                        slack.post(error)
                      }
                    },

                    export: async function (indexName) {
                        try {
                          index = client.initIndex(indexName);

                          var browser = index.browseAll();
                          var hits = [];

                          browser.on("result", function onResult(content) {
                            hits = hits.concat(content.hits);
                          });

                          browser.on("end", function onEnd() {
                            console.log("Finished!");
                            console.log(`We got ${hits.length} hits`);
                            hits = {
                              result: hits
                            };
                            fs.writeFile(`./data/_backup/algolia${indexName}Backup.json`, JSON.stringify(hits, null, 2));
                          });

                          browser.on("error", function onError(err) {
                            throw err;
                          });

                          console.log(`Exported ${indexName}`);

                          return hits
                        } catch (error) {
                          console.log(error);
                          slack.post(error)
                        }

                      },

                      deleteResult: function (IDs, indexName) {
                        try {
                          //change to correct index based on if its a technology or content
                          index = client.initIndex(indexName);;
                          index.deleteObjects(IDs, function (err, content) {
                            console.log(content);
                          });
                        } catch (error) {
                          console.log(error);
                          slack.post(error)
                        }
                      },

                      updateResultsPartly: async function (fieldname, newValue) {
                          try {
                            const fs = require("fs");
                            //read algolia backup from file to variable
                            let AlgoliaContent = JSON.parse(
                              fs.readFileSync(`./data/_backup/algoliaSkillMeBackup.json`)
                            );
                            let AlgoliaTopics = JSON.parse(
                              fs.readFileSync(`./data/_backup/algoliaSkillMeTopicsBackup.json`)
                            );


                            //for every index
                            let indexes = [{
                              name: "SkillMeBackup",
                              file: AlgoliaContent
                            }, {
                              name: "SkillMeTopicsBackup",
                              file: AlgoliaTopics
                            }]
                            for (indexObject of indexes) {
                              //change index
                              index = client.initIndex(indexObject.name);;

                              //for every result in indexObject.file
                              for (result of indexObject.file.result) {

                                //overwrite fieldname with newValue and send it to algolia
                                result[fieldname] = newValue

                                //make result smaller by deleting all not needed fields
                                for (field in result) {
                                  if (field != "objectID" && field != fieldname) {
                                    delete result[field]
                                  }
                                }
                              }

                              index.partialUpdateObjects(indexObject.file.result, function (err, content) {
                                if (err) throw err;
                                console.log(content);
                              });


                            }


                          } catch (error) {
                            console.log(error);
                            slack.post(error)
                          }
                        },

                        correctResultforAlgolia: async function (result) {
                            try {
                              if (result.relatedTopics) {
                                if (result.relatedTopics.length > 0) {
                                  var relatedTopics = result.relatedTopics.map(topic => topic.name)
                                } else {
                                  var relatedTopics = null
                                }
                              }

                              if (result.SimilarToolsArray) {
                                if (result.SimilarToolsArray.length > 0) {
                                  var SimilarToolsArray = result.SimilarToolsArray.map(tool => tool.name)
                                } else {
                                  var SimilarToolsArray = null
                                }
                              }

                              if (result.CompaniesUsingItArray) {
                                if (result.CompaniesUsingItArray.length > 0) {
                                  var CompaniesUsingItArray = result.CompaniesUsingItArray.map(company => company.name)
                                } else {
                                  var CompaniesUsingItArray = null
                                }
                              }

                              var result = {
                                name: result.name,
                                Recommended: result.Recommended || null,
                                synonyms: result.synonyms || null,
                                slug: result.slug || null,
                                by: result.by || null,
                                typeName: result.typeName || null,
                                contentcategory: result.contentcategory || null,
                                topiccategory: result.topiccategory || null,
                                typeNameonStackshare: result.typeNameonStackshare || null,
                                votesNumber: result.votesNumber || null,
                                sortvalue: result.sortValue || null,
                                integrationsNumber: result.integrationsNumber || null,
                                GitHubStars: result.GitHubStars || null,
                                GitHubForks: result.GitHubForks || null,
                                fansNumber: result.fansNumber || null,
                                stacksNumber: result.stacksNumber || null,
                                releasedate: result.releaseDate || null,
                                country: result.country || null,
                                city: result.city || "none",
                                state: result.state || null,
                                _geoloc: result._geoloc ? {
                                  lat: result.lat,
                                  lng: result.lon
                                } : null,
                                eventUNIXtime: result.eventUNIXtime || 4102358400,
                                eventUNIXtimeEND: result.eventUNIXtimeEND ? result.eventUNIXtimeEND : result.eventUNIXtime ? result.eventUNIXtime != 4102358400 ? (result.eventUNIXtime + (result.timeMinutes * 60)) : 4102358400 : 4102358400,
                                UTCoffset: result.UTCoffset || null,
                                eventVenue: result.eventVenue || null,
                                meetupGroup: result.meetupGroup || null,
                                meetupID: result.meetupID || null,
                                meetupurlname: result.meetupurlname || null,
                                eventRSVPlimit: result.eventRSVPlimit || null,
                                eventPlacesLeft: result.eventPlacesLeft || null,
                                eventRSVPs: result.eventRSVPs || null,
                                eventRSVPsYES: result.eventRSVPsYES || null,
                                eventRSVPsMAYBE: result.eventRSVPsMAYBE || null,
                                members: result.members || null,
                                eventWaitlist: result.eventWaitlist || null,
                                releaseYYYY: result.releaseYYYY || null,
                                ifTechnology: result.IFtechnology || null,
                                headline: result.headline || null,
                                description: result.descriptionExtended || result.description || null,
                                descriptionSourceName: result.descriptionSourceName == "InLearnity" ? "SkillMe" : result.descriptionSourceName ? result.descriptionSourceName : null,
                                descriptionSourcePictureURL: result.descriptionSourceName == "InLearnity" ? "images/guideface.jpg" : result.descriptionSourcePictureURL ? result.descriptionSourcePictureURL : null,
                                image: result.image || null,
                                meetuptopics: result.meetuptopics || null,
                                relatedTopics: relatedTopics || null,
                                SimilarTools: SimilarToolsArray || null,
                                CompaniesUsingIt: CompaniesUsingItArray || null,
                                IFcontentSource1: result.IFcontentSource1 || null,
                                contenturl: result.contenturl || null,
                                IFcontentSource1PictureURL: result.IFcontentSource1PictureURL || null,
                                WikipediaURL: result.WikipediaURL || null,
                                stackshareURL: result.stackshareURL || null,
                                website: result.website || null,
                                GitHub: result.GitHub || null,
                                FavoriteNumber: result.FavoriteNumber || null,
                                highlight: result.highlight || null,
                                bookmarkedNumber: result.bookmarkedNumber || null,
                                levelName: result.levelName || null,
                                projectsNumber: result.projectsNumber || null,
                                PriceUSD: result.PriceUSD || null,
                                PriceRange: result.PriceRange || null,
                                timeMinutes: result.timeMinutes || null,
                                timeUnitName: result.timeUnitName || null,
                                time: result.time || null,
                                averageRating: result.averageRating ? parseFloat(result.averageRating) : null,
                                numberOfRatings: result.numberOfRatings || null,
                                numberOfLectures: result.numberOfLectures || null,
                                numberOfQuizzes: result.numberOfQuizzes || null,
                                BestSeller: result.BestSeller || null,
                                BestSellerIn: result.BestSellerIn || null,
                                BestSellerRankInBooks: result.BestSellerRankInBooks || null,
                                StudentsEnrolled: result.StudentsEnrolled || null,
                                HoursOnDemandVideo: result.HoursOnDemandVideo || null,
                                booktype: result.booktype || null,
                                PaperbackPages: result.PaperbackPages || null,
                                ISBN10: result.ISBN10 || null,
                                RSVPnumber: result.RSVPnumber || null,
                                upvotes: result.upvotes || null,
                                downvotes: result.downvotes || null,
                                views: result.views || null,
                                WhyPeopleLikeItReasons: result.WhyPeopleLikeItReasons ? result.WhyPeopleLikeItReasons.map(reason => reason.Reason) : null,
                                YouTubeID: result.YouTubeID || null,
                                YouTubeTrailerID: result.YouTubeTrailerID || null,
                                VimeoTrailerID: result.VimeoTrailerID || null,
                                Options: result.Options || null,
                                appstoreID: result.appstoreID || null,
                                icon: result.icon || null,
                                primaryGenre: result.primaryGenre || null,
                                contentRating: result.contentRating || null,
                                languages: result.languages || null,
                                sizeinMB: result.sizeinMB || null,
                                installs: result.installs || null,
                                requiredOsVersion: result.requiredOsVersion || null,
                                genres: result.genres || null,
                                developerId: result.developerId || null,
                                developer: result.developer || null,
                                developerUrl: result.developerUrl || null,
                                developerWebsite: result.developerWebsite || null,
                                screenshots: result.screenshots || null,
                                ipadScreenshots: result.ipadScreenshots || null,
                                supportedDevices: result.supportedDevices || null,
                              }

                              async function cleanNull() {
                                //delete null fields in result before pushing to Algolia => better overview & faster loading
                                for (var propName in result) {
                                  if (result[propName] === null || result[propName] === undefined || typeof result[propName] == "object" && result[propName].length == 0) {
                                    delete result[propName];
                                  }
                                }
                              }

                              await cleanNull();
                            } catch (error) {
                              console.log(error);
                              slack.post(error);

                            }

                            return result
                          },

                          createResult: async function (result) {
                              try {
                                var start = now();

                                console.log(`Start: Import to Algolia - "${result.name}"`);

                                console.log(result)
                                result = await module.exports.correctResultforAlgolia(result)

                                console.log(result)

                                //if content, then add to content index, otherwise add to topic index
                                if (result.contentcategory) {
                                  index = client.initIndex("SkillMeBackup");
                                } else {
                                  index = client.initIndex("SkillMeTopicsBackup");
                                }

                                let ignoreCategory = [
                                  "topic category",
                                  "content category",
                                  "source",
                                  "topic",
                                  "organization",
                                  "project type",
                                ]

                                //only allow content and technologies
                                if (ignoreCategory.includes(result.typeName)) {
                                  result.id_algolia = "not on Algolia"
                                } else {
                                  index.addObject(result,
                                    function (err, content) {
                                      try {
                                        result.id_algolia = content.objectID;
                                        console.log(content.objectID)
                                        console.log(`Success: Imported to Algolia - "${result.name}"`);
                                      } catch (error) {
                                        //send message to slack/errorhandler - couldnt add to algolia
                                        console.log(error);
                                        slack.post(error)
                                      }

                                    }
                                  );

                                  await wait(1500);
                                }

                                return result.id_algolia
                              } catch (error) {
                                console.log(error);
                                slack.post(error)
                              }
                            },
                            clearIndex: async function (indexname) {
                                index = client.initIndex(indexname);
                                index.clearIndex(function (err, content) {
                                  console.log(content);
                                });
                              },
                              copyIndex: async function (from, to) {
                                client.copyIndex(from, to, function (err, content) {
                                  if (err) throw err;

                                  console.log(content);
                                });
                              }
};