// created by
// Marco Bartsch

const {
    promisify
} = require("util");
const algolia = require("./algolia.js");
const slack = require("./slack.js");
const sitemapGenerator = require("./sitemapGenerator.js");

function wait(ms) {
    return new Promise(resolve => {
        setTimeout(() => resolve(), ms);
    });
}

importALL()

///////////////////////////////////////////////////
// import ALL results
///////////////////////////////////////////////////

async function importALL() {
    try {
        // backup algolia indexes first!
        await algolia.export("SkillMe");
        await algolia.export("SkillMeTopics");

        await wait(20000)

        // make Algolia backup indexes empty
        await algolia.clearIndex("SkillMeBackup");
        await algolia.clearIndex("SkillMeTopicsBackup");

        await wait(5000)

        await importdata("./data/SkillMeTopics.json", "SkillMeTopicsBackup");
        await importdata("./data/SkillMe.json", "SkillMeBackup");

        await wait(10000)

        await algolia.copyIndex("SkillMeBackup", "SkillMe")
        await algolia.copyIndex("SkillMeTopicsBackup", "SkillMeTopics")

        await sitemapGenerator.updateSitemap()
        console.log("Updated Algolia database");
        slack.post("Updated Algolia database");
    } catch (error) {
        console.log(error);
        slack.post(error);
    }
}

async function importdata(where, indexName) {
    try {
        console.log(`Start importing: ${where}`);
        slack.post(`Start importing: ${where}`)
        var results = require(where)

        for (var i = 0; i < results.length; i++) {
            try {
                results[i] = await algolia.correctResultforAlgolia(results[i]);
            } catch (error) {
                console.log(error)
            }

            //wait for latest result, then import largefile
            if (i == (results.length - 1)) {
                //import file via batch import
                console.log(`Import ${where} to Algolia...`);
                slack.post(`Import ${where} to Algolia...`);
                await algolia.importLargeFile(results, indexName);
                console.log(`Finished import ${where} to Algolia!`);
                slack.post(`Finished import ${where} to Algolia!`);
            }
        }


    } catch (error) {
        console.log(error)
        // errorhandler.push("addToWebflowAndAlgolia.js", "handleAddtoWebflowAndAlgolia", error)
    }
}