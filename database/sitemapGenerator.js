// created by
// Marco Bartsch

// based on https://github.com/algolia/algolia-sitemap

const algoliaSitemap = require('algolia-sitemap');
const config = require("./config");
const indexes = ["SkillMe", "SkillMeTopics"]


module.exports = {
    updateSitemap: async function () {
        indexes.forEach(
            async (index) => {
                if (index == "SkillMe") {
                    var folder = "result"
                } else {
                    var folder = "tech"
                }
                const algoliaConfig = {
                    appId: config.Algolia_ApplicationID,
                    apiKey: config.Algolia_APIkeyBrowseOnly, // make sure the key has "browse" capability
                    indexName: index,
                };

                algoliaSitemap({
                    algoliaConfig,
                    sitemapLoc: 'https://heyskill.me/sitemaps',
                    outputFolder: 'docs/sitemaps',
                    hitToParams,
                });

                function hitToParams({
                    objectID,
                    slug,
                    image,
                    name,
                }) {
                    const url = ({
                            objectID
                        }) =>
                        `https://heyskill.me/${folder}?q=${slug}`;
                    const loc = url({
                        lang: 'en',
                        objectID
                    });
                    const lastmod = new Date().toISOString();
                    const priority = Math.random();
                    if (image.includes("images/")) {
                        return {
                            loc,
                            lastmod,
                            priority,
                        };
                    } else {
                        return {
                            loc,
                            lastmod,
                            priority,
                            images: [{
                                loc: image,
                                title: name,
                            }],
                        };
                    }
                }

                /**
                 * @typedef {Object} Params
                 * @property {string} loc the link of this hit
                 * @property {string} [lastmod] the last time this link was modified (ISO8601)
                 * @property {number} [priority] the priority you give to this link (between 0 and 1)
                 * @property {Object} [alternates] alternative versions of this link (useful for multi-language)
                 * @property {Array} [alternates.languages] list of languages that are enabled
                 * @property {Array} [images] list of images links related to the hit
                 * @property {function} [alternates.hitToURL] function to transform a language into a url of this object
                 */
            }
        )
    }
}