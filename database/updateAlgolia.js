// created by
// Marco Bartsch

const addToAlgolia = require("./addToAlgolia.js");
const algolia = require("./algolia.js");
const slack = require("./slack.js");
const cron = require("node-cron");

// trigger every sunday at 0:00
importToAlgolia()
cron.schedule("0 0 * * 0", async function () {
    importToAlgolia()
})


async function importToAlgolia() {
    try {
        //make database backup before changes
        await algolia.export("SkillMe");
        await algolia.exportSynonyms("SkillMe");
        await algolia.exportQueryRules("SkillMe");
        await algolia.export("SkillMeTopics");
        await algolia.exportSynonyms("SkillMeTopics");
        await algolia.exportQueryRules("SkillMeTopics");
        await algolia.export("search queries");
        await algolia.exportSynonyms("search queries");
        await algolia.exportQueryRules("search queries");
        await algolia.export("specialHints");
        await algolia.exportSynonyms("specialHints");
        await algolia.exportQueryRules("specialHints");

        await wait(60000);

        //delete all results on algolia: SkillMeBackup & SkillMeTopicsBackup
        await algolia.clearIndex("SkillMeTopicsBackup");
        await algolia.clearIndex("SkillMeBackup");

        //import all results 
        await addToAlgolia.importALL(); // ✅ all fine (?)

        await wait(120000)

        //copy on Algolia: SkillMeBackup to SkillMe & SkillMeTopicsBackup to SkillMeTopics
        await algolia.copyIndex("SkillMeTopicsBackup", "SkillMeTopics");
        await algolia.copyIndex("SkillMeBackup", "SkillMe");
        slack.post("Finished updating Algolia database!")
    } catch (error) {
        console.log(error)
    }
}