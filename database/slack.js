// created by
// Marco Bartsch

var config = require("./config");
var Slack = require("slack-node");
webhookUri = `https://hooks.slack.com/services/${config.Slack_URI}`;

slack = new Slack();
slack.setWebhook(webhookUri);

module.exports = {
  post: function (message, errors) {
    try {
      if (message.includes("✅ Draft") || message.includes("✅ Sent") || message.includes("✅ Published") || message.includes("✅ Updated blog post") || message.includes("Weekly Update") || message.includes("Daily Update") || message.includes("Monthly Update") || message.includes("🚀 Wow,") || message.includes("⚠️ Oh...") || message.includes("🎉 ")) {
        slack.webhook({
            channel: "#marketing",
            username: "Node.js Social Media Script",
            text: message
          },
          function (err, response) {}
        );
      } else if (errors === undefined) {
        slack.webhook({
            channel: "#alerts",
            username: "Node.js process",
            text: message
          },
          function (err, response) {}
        )
      } else {
        slack.webhook({
            channel: "#alerts",
            username: "Node.js process",
            text: message,
            attachments: [{
              fallback: "Required plain-text summary of the attachment.",
              color: "#36a64f",
              author_name: errors[0].source,
              title: errors[0].error.name,
              text: errors[0].error.summary,
              fields: [{
                title: "Error",
                value: errors[0].error.details,
                short: false
              }]
            }]
          },
          function (err, response) {}
        );
      }
    } catch (error) {
      console.log(error)
      slack.webhook({
          channel: "#alerts",
          username: "Node.js process",
          text: error
        },
        function (err, response) {}
      )
    }

  }
};