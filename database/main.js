// created by
// Marco Bartsch

const appstore = require("./getData/appstore.js");
const meetup = require("./getData/meetupAPI.js");
const addToAlgolia = require("./addToAlgolia.js");
const cron = require("node-cron");
const UdemyAPI = require("./getData/UdemyAPI.js");
const now = require("performance-now");
const algolia = require("./algolia.js");
const slack = require("./slack.js");