// created by
// Marco Bartsch

const fs = require("fs");
const config = require("./config.json")
const correctResult = require("./getData/correctResult")
const slack = require("./slack.js");
const topicSources = []
const contentSources = []
for (source of config.topics) {
    topicSources.push(source[0])
}
for (source of config.sources) {
    contentSources.push(source[0])
}
// const sources = config.topicSources & config.sources + indexName (SkillMe / SkillMeTopics)
const indexes = [{
    index: "SkillMe",
    sources: contentSources
}, {
    index: "SkillMeTopics",
    sources: topicSources
}]

function wait(ms) {
    return new Promise(resolve => {
        setTimeout(() => resolve(), ms);
    });
}

const wrongUsage = config.database_wrongUsage

var matchTopics = []


alwaysUpdate()

async function alwaysUpdate() {
    for (;;) {
        await add()
    }
}

async function buildMatchTopics() {
    // Step 1: build matchTopics.json: 
    // for every topic file, 
    await topicSources.forEach(
        async (filename) => {
            file = require(`./data/_unfinished/${filename}`)
            // for every result of that file: 
            file.result.forEach(
                async (result) => {
                    // check if result already exists in matchTopics.json (based on name)
                    if (matchTopics.includes((result).name.toLowerCase()) == false) {
                        result = await correctResult.correct(result);

                        // if not: add to list
                        matchTopics.push({
                            name: (result).name.toLowerCase(),
                            synonyms: result.synonyms ? result.synonyms : null,
                            sortValue: result.sortValue ? result.sortValue : null
                        })
                    }
                }
            )
        }
    )
    await wait(5000)
    console.log("Completed: Step 1: build matchTopics.json")
}

async function add() {
    slack.post("Started databaseUpdater")
    // Step 1: build matchTopics.json: 
    await buildMatchTopics()
    // Step 2: add topics & content
    for (indexx of indexes) {
        var indexFile = []
        var index = indexx.index
        //  - for every source of sources, 
        for (source of indexx.sources) {
            try {
                var sourcefile = require(`./data/_unfinished/${source}.json`)

                //make sure file layout is correct: file.result
                if (sourcefile.length > 2) {
                    sourcefile = {
                        result: sourcefile
                    }
                }

                // for every result of that source: 
                for (result of sourcefile.result) {
                    try {
                        //check if resultname exists
                        if (result.name) {
                            var alreadyExists = function (element) {
                                return (result.slug == element.slug)
                            };
                            // check if result already exists in [source.index].json (based on .some => slug)
                            // if not: 
                            if (indexFile.some(alreadyExists) == false) {
                                // use correctResult(result) to create fixed layout
                                result = await correctResult.correct(result)
                                result = await correctResult.deleteNullFields(result)
                                result.relatedTopics = []

                                // extract related topics: 
                                // for every word in matchTopics:
                                for (word of matchTopics) {
                                    // check if word is included in "wrongUsage" scenario
                                    var wordDangerous = function (element) {
                                        return word.name == element.word.toLowerCase()
                                    };
                                    var dangerousWord = wrongUsage.find(wordDangerous)
                                    if (dangerousWord) {

                                        // for every wrong scenario of a dangerous word...
                                        for (var i = 0; i < dangerousWord.wrong.length; i++) {
                                            // check if scenario exists in text, if true then break the loop and do nothing
                                            if ((await matchTopic(result, dangerousWord.wrong[i])) == true) {
                                                break
                                            }
                                            if (i + 1 == dangerousWord.wrong.length) {
                                                // if loop completed: check if word exists 
                                                if ((await matchTopic(result, word.name)) == true) {
                                                    // if matchTopic == true: add topic (name + sortValue) to relatedTopics
                                                    if (word.name != result.name.toLowerCase()) {
                                                        result.relatedTopics.push(word)
                                                    }
                                                }
                                            }

                                        }
                                    } else {
                                        // if false: function matchTopic(result,matchWord)
                                        if ((await matchTopic(result, word.name)) == true) {
                                            // if matchTopic == true: add topic (name + sortValue) to relatedTopics
                                            if (word.name != result.name.toLowerCase()) {
                                                result.relatedTopics.push(word)
                                            }
                                        } else {
                                            // else check synonyms
                                            if (word.synonyms) {
                                                for (synonym of word.synonyms) {
                                                    if ((await matchTopic(result, synonym)) == true) {
                                                        // if matchTopic == true: add topic (name + sortValue) to relatedTopics
                                                        if (word.name != result.name.toLowerCase()) {
                                                            result.relatedTopics.push(word)
                                                            break
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }

                                if (result.relatedTopics) {
                                    result.relatedTopics.sort(function (a, b) {
                                        if (a.sortValue > b.sortValue) return -1;
                                        if (a.sortValue < b.sortValue) return 1;
                                        return 0;
                                    });
                                }

                                // add result to [source.index].json
                                indexFile.push(result)
                                console.log(`Added: ${JSON.stringify(result.name, null, 2)}`)
                                // if less then 100 results, save to preview folder
                                if (indexFile.length < 100) {
                                    fs.writeFileSync(`./data/_preview/${index}.json`, JSON.stringify(indexFile, null, 2));
                                }
                            }
                        }
                    } catch (error) {
                        console.log(error);
                        slack.post(error)
                    }
                }

            } catch (error) {
                console.log(error);
                slack.post(error)
            }
        }
        // save file
        fs.writeFile(`./data/${index}.json`, JSON.stringify(indexFile, null, 2));
    }
    slack.post("Finished databaseUpdater!")
}

// Output: JSON files for all sources with latest data, including extracted related Topics and no duplicates




async function matchTopic(result, matchWord) {
    var match = false
    var wordConnector = config.database_wordConnector

    if ((await matchWordVersion(result, matchWord)) == true) {
        // if match: match = true
        match = true
    } else {
        // if no match: 
        // for loop: as long as i smaller then wordConnector.length
        for (var i = 0; i < wordConnector.length; i++) {
            // check if matchWord contains wordConnector[i]
            if (matchWord.includes(wordConnector[i].connector) && wordConnector[i].regex) {
                for (otherconnector of wordConnector) {
                    // if true: matchWordVersion(result,matchWord without wordConnector[i] and with another wordConnector[i]) 
                    matchWord = matchWord.replace(wordConnector[i].regex, otherconnector.connector)
                    if ((await matchWordVersion(result, matchWord)) == true) {
                        // if match: 
                        match = true
                        break
                    }
                }
                if (match == true) {
                    break
                }
            }
        }
    }
    return match
}

async function matchWordVersion(result, wordVersion) {
    var found = false
    var wordEndings = config.database_wordEndings

    // don't add topic itself in related topics
    if (wordVersion.toLowerCase() != result.name.toLowerCase()) {
        // check all fields of the result
        for (var prop in result) {
            // check if wordVersion.toLowerCase is included in fieldsToCheck[i].toLowerCase
            if (result[prop]) {
                if (result[prop].toString().length > 0) {
                    for (var a = 0; a < wordEndings.length; a++) {
                        if ((result[prop].toString().toLowerCase()).includes(` ${wordVersion.toLowerCase()}${wordEndings[a]}`) ||
                            (result[prop].toString().toLowerCase()).includes(`(${wordVersion.toLowerCase()}${wordEndings[a]}`) ||
                            (result[prop].toString().toLowerCase()).includes(`"${wordVersion.toLowerCase()}${wordEndings[a]}"`) ||
                            (result[prop].toString().toLowerCase()).startsWith(`${wordVersion.toLowerCase()}${wordEndings[a]}`) ||
                            (result[prop].toString().toLowerCase()) == wordVersion.toLowerCase()) {
                            // if match: 
                            found = true
                            break
                        }
                    }
                }
            }
        }
    }
    return found
}