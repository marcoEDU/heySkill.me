// created by
// Marco Bartsch

var striptags = require("striptags");

module.exports = {
  return: function(text) {
    //if description text is short, save it without HTML tags, otherwise shorten it first, then save
    function getWords(str) {
      return str
        .split(/\s+/)
        .slice(0, 55)
        .join(" ");
    }

    function getWordsLong(str) {
      return str
        .split(/\s+/)
        .slice(0, 1500)
        .join(" ");
    }

    if (
      striptags(text)
        .replace(/\./g, ". ")
        .replace(/\?/g, "? ")
        .replace(/ {2,}/g, " ").length ==
      getWords(striptags(text))
        .replace(/\./g, ". ")
        .replace(/\?/g, "? ")
        .replace(/ {2,}/g, " ").length
    ) {
      var newText = getWords(striptags(text))
        .replace(/\./g, ". ")
        .replace(/\?/g, "? ")
        .replace(/ {2,}/g, " ");
      var shortened = false;
    } else {
      var newText = getWords(striptags(text))
        .replace(/\./g, ". ")
        .replace(/\?/g, "? ")
        .replace(/ {2,}/g, " ");
      var shortened = true;
    }
    newText = module.exports.noHTMLnoWrongSpaces(newText);

    return { newText: newText, shortened: shortened };
  },
  noHTMLnoWrongSpaces: function(text) {
    //delete wrong spaces at beginning and end
    var newText = striptags(text);
    newText = newText.replace(/^\s\s*/, "").replace(/\s\s*$/, "");

    return newText;
  }
};
