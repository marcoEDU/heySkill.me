# SkillMe - Your guide to learn to code

✅ Online: https://heyskill.me

## How can I help / contribute?
Want to help? That's awesome! If you are experienced in software development or marketing - or you know someone who is - drop me a mail with some references / projects and how you want to help, to marco@heyskill.me. Happy to hear from you!

Found a bug? Please submit it as an issue - that would be incredibly helpful! https://github.com/marcoEDU/heySkill.me/issues


## What is SkillMe?
<img src="docs/images/blog_learntocode_header.jpg"/>
SkillMe is a guide to help people learn to code. By helping you getting started with learning to code and giving you personalized recommendations. Explore all the best learning sources and technologies on ONE website!

### What sources does SkillMe include?

- [Amazon (books)](https://heyskill.me/search?q=Amazon) 
- [Udacity (online courses & nanodegrees)](https://heyskill.me/search?q=Udacity) 
- [Udemy (online courses)](https://heyskill.me/search?q=Udemy) 
- [Meetup (for meetups & groups)](https://heyskill.me/search?q=Meetup) 
- [Apple App Store (iOS apps)](https://heyskill.me/search?q=Apple) 
- Stackshare (programming languages, frameworks & other technologies)
- ... many more are coming


### What does the code include?
- frontend
- database updater
  - getting the latest metadata (name, description, picture, price, etc.) from my Parsehub scrapers (using the Parsehub API) & APIs
  - update local JSON files if results (online courses, books, etc.) changed
  - extract from result fields (name, description, etc.) related programming languages, frameworks + other technologies and topics -> to make content & topics better explorable and give better recommendations to users
  - push data to database via Algolia API
- marketing scripts (to automatically send posts I created in Airtable -> makes marketing easier and more efficient)


## What comes next?
Take a look on https://github.com/marcoEDU/heySkill.me/projects


## Created by
Me, [marcoEDU](https://github.com/marcoEDU) (UX/UI designer who recently started learning to code, while building up SkillMe to help other people learn to code as well), supported by some amazing friends and fellow hackers @[Noisebridge](https://www.noisebridge.net/wiki/Noisebridge) in San Francisco & @[AFRA](https://afra-berlin.de) & @[Motionlab](https://motionlab.berlin) in Berlin.

### Code quality
This project is the first I ever coded - I actually learned programming at Noisebridge (the most awesome hackerspace, in San Francisco) while building up this platform to help people learn to code (ironic?). Started with backend development and inspired by a friend I decided to work with JavaScript & Node... oh well... Python might have been a better choice, but it worked out ok so far. Learned a lot about coding while building up the backend. Since my background used to be UI/UX design, I created the frontend in Webflow - and recently got completely away from Webflow -> so for the first time I needed to write/modify frontend code. Yeah... the code is messy... but: doing its job and ok for a first release.
For the next big release: yeah... a new code base sounds like a good idea.