# SkillMe - Your guide to learn to code
by Marco Bartsch, updated 2018 September 10


## Business Model Canvas

### Key Partners
- **Key suppliers:**
Learning resources (Udacity, Udemy, Amazon, Meetup, etc.)
=> get learning content via official & unofficial APIs + scraper
- **Motivation for partners:**
Getting more users via SkillMe

### Key Activities
- keep website running
- improving platform based on user feedback
- extending sources & features
- communication via social media, promotion on co-learning events, promotion at universities and other locations, newsletter and more

### Key Resources
- website with great usability
- discoverability
- team (software developers & marketing mostly)
- money for team & hosting

### Value Propositions
- Easy entry in learning to code for beginners
- All the best learning resources on one website
- better learning success via personalized learning recommendations

### Customer Relationships
**Beginner dev:**
- friendly guide / mentor who helps getting started

**Experienced dev:**
- efficient search for learning content

### Channels
- social media
- Google
- meetups via Meetup.com
- offline advertising
- online banner

### Customer Segments
- **Focus:** beginner who want to learn software development or who just started (probably not high income)
- **Secondary:** already experienced developers who want to extend their knowledge (more likely to already have some money to spend from previous jobs)

### Cost Structure
- team (salary)
- office
- search engine (Algolia) + other software

### Revenue Streams
- affiliate programs from sources
- (later) paid highlighted results
- (later) paid plans with more features & possibly discount at sources