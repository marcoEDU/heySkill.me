## Brand
SkillMe

## Tagline
Your guide to learn to code

## URL
https://heySkill.me

## Elevator pitch
(I created a learning platform called SkillMe)
HeySkill.me is the only website you need to know, to get started with learning software development & finding the best content to do so. 

Guide articles to learn to code + all the best learning content with everything from online courses on Udacity, books on Amazon to meetups on Meetup.com + a super flexible search + personalized learning recommendations. HeySkill.me is NOT just a search engine for learning content, but YOUR GUIDE to learn to code - whether your a beginner or experienced developer.


## Company story
Learning to code was never as important as it is now. And the demand for great developers will continue to increase the coming years - what resulted over the last years in more and more sources for learning content to learn an increasingly complex diversity of technologies.
All this makes beginner worry - how to get started with learning to code? What technologies should I learn and with what learning resources?
It's time for a central starting point for everyone who wants to learn to code or extend coding knowledge - a single website for both beginners and experienced developers. Introducing SkillMe - Your guide to learn to code.

## Team story
- **Marco
(Founder, designer, developer)**
    - UX/UI design background
    - started learning to code while building up SkillMe - to help others learning to code as well
    - focus on empower people (especially minority groups) to find their passion and unleash their creative potential - making a living doing what they love doing