// created by
// Marco Bartsch

function getRelativeTime(eventUNIXstart, timeMinutes) {
    var nowUNIX = Math.round((new Date()).getTime() / 1000) //seconds since 1970

    //if event start doesn't exists =>  relativeTime=null
    var relativeTime = null;
    var timeSeconds = timeMinutes ? timeMinutes * 60 : 0;
    var eventUNIXend = eventUNIXstart + timeSeconds;

    if (eventUNIXstart) {
        //if event beginning (eventUNIXstart) is less (earlier) then nowUnix => already started, maybe finished
        if (eventUNIXstart < nowUNIX) {
            ////if event ending (eventUNIXend) is less (earlier) then nowUnix => event finished => relativeTime="past event"
            if (eventUNIXend < nowUNIX) {
                relativeTime = "past event"
            }
            ////Else if event ending (eventUNIXend) is more (later) then nowUnix => event ongoing => relativeTime="right now!"
            else if (eventUNIXend > nowUNIX) {
                relativeTime = "right now!"
            }
        }
        //Else if event beginning (eventUNIXstart) is more (later) then nowUnix => event in the future
        else if (eventUNIXstart > nowUNIX) {
            ////calculate difference between event beginning (eventUNIXstart) & nowUnix
            var timediff = eventUNIXstart - nowUNIX
            ////if difference is < 60 minutes => relativeTime="in XX min"
            if (timediff < (60 * 60)) {
                relativeTime = `in ${(timediff/60).toFixed(0)} min`
            }
            ////Else if difference is < 24 hours => relativeTime="in XX hour/s"
            else if (timediff < ((60 * 60) * 24)) {
                relativeTime = `in ${((timediff/60)/60).toFixed(0) == 1 ? "1 hour" : ((timediff/60)/60).toFixed(0)+" hours"}`
            }
            ////Else if difference is < 14 days => relativeTime="in XX day/s"
            else if (timediff < (((60 * 60) * 24) * 14)) {
                relativeTime = `${(((timediff/60)/60)/24).toFixed(0) == 1 ? "tomorrow" : "in "+(((timediff/60)/60)/24).toFixed(0)+" days"}`
            }
            ////Else if difference is < 8 weeks => relativeTime="in XX weeks"
            else if (timediff < ((((60 * 60) * 24) * 7) * 8)) {
                relativeTime = `in ${((((timediff/60)/60)/24)/7).toFixed(0) == 1 ? "1 week" : ((((timediff/60)/60)/24)/7).toFixed(0)+" weeks"}`
            }
            ////Else if difference is < 12 months => relativeTime="in XX months"
            else if (timediff < ((((60 * 60) * 24) * 30) * 12)) {
                relativeTime = `in ${((((timediff/60)/60)/24)/30).toFixed(0) == 1 ? "1 month" : ((((timediff/60)/60)/24)/30).toFixed(0)+" months"}`
            }
            ////Else if difference is > 12 months => relativeTime="in more then 1 year"
            else if (timediff > ((((60 * 60) * 24) * 30) * 12)) {
                relativeTime = "in more then 1 year"
            }
        }
    }

    return relativeTime
}

function getHHMM(UNIXtime, UTCoffset) {
    var now = new Date();
    var timezoneNow = -(now.getTimezoneOffset() * 60);

    //if UTCoffset in milliseconds, change to seconds
    if (UTCoffset > 1000000 || UTCoffset < -1000000) {
        UTCoffset = (UTCoffset / 1000)
    }
    //if timezone not current timezone => adapt UNIXtime based on timezome
    if (UTCoffset == timezoneNow) {
        UNIXtime = UNIXtime
    } else {
        UNIXtime = UNIXtime + UTCoffset - timezoneNow
    }


    let date = new Date(UNIXtime * 1000);

    if (date.getHours() > 0) {
        var HHMM = `${date.getHours()<10?"0"+date.getHours():date.getHours()}:${date.getMinutes()<10?"0"+date.getMinutes():date.getMinutes()}`
    } else {
        var HHMM = null
    }

    return HHMM
}

function getDDMMMYYYY(UNIXtime) {
    var a = new Date(UNIXtime * 1000);
    var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    var year = a.getFullYear();
    var month = months[a.getMonth()];
    var date = a.getDate();
    var time = date + ' ' + month + ' ' + year;
    return time;
}

function getTodayBeg() {
    var today = new Date();
    var dayNow = today.getDate();
    var monthNow = today.getMonth(); //January is 0!
    var yearNow = today.getFullYear();

    var datum = new Date(Date.UTC(yearNow, monthNow, dayNow, '00', '00', '00'));

    return (datum.getTime() / 1000)
}

function getTodayEnd() {
    var today = new Date();
    var dayNow = today.getDate();
    var monthNow = today.getMonth(); //January is 0!
    var yearNow = today.getFullYear();

    var datum = new Date(Date.UTC(yearNow, monthNow, dayNow, '23', '59', '59'));

    return (datum.getTime() / 1000)
}